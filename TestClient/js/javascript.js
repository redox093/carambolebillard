let container = null

let login_btn = null
let logout_btn = null
let creerPartie_btn = null

let in_partie = false;

window.onload = ()=>{
	container = document.getElementById("container")
	login_btn = document.getElementById("login")
	logout_btn = document.getElementById("logout")
	creerPartie_btn = document.getElementById("creerPartie")
}

function login(){
	let nom = document.getElementById("nom").value
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "connexion",
			nom : nom
		}
	})
	.done(response => {
		//response = JSON.parse(response);
		console.log(response);
		
	});
}

function logout(){
	let nom = document.getElementById("nom").value
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "disconect",
			nom : nom
		}
	})
	.done(response => {
		//response = JSON.parse(response);
		console.log(response);
		in_partie = false;
	});
}

function creerPartie(type){
	let nom = document.getElementById("nom").value
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "creerPartie",
			nom : nom,
			typePartie : type
		}
	})
	.done(response => {
		//response = JSON.parse(response);
		console.log(response);
		if(response.message == "ATTENTE_ADVERSAIRE"){
			setTimeout(statusDemande,2000)
		}
		else if(response.message == "TROUVEE_ADVERSAIRE"){
			in_partie = true;
			setTimeout(statusPartie,2000)
		}
	});
}

function statusDemande(){
	let nom = document.getElementById("nom").value
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "statusDemande",
			nom : nom
		}
	})
	.done(response => {
		//response = JSON.parse(response);
		console.log(response);
		if(response.message == "ATTENTE_ADVERSAIRE"){
			setTimeout(statusDemande,2000)
		}
		else if(response.message == "TROUVEE_ADVERSAIRE"){
			in_partie = true;
			setTimeout(statusPartie,2000)
		}
	});
}

function statusPartie(){
	if(in_partie){
		let nom = document.getElementById("nom").value
		let adress = getAdress()
		let x = document.getElementById("posx").value
		let y = document.getElementById("posy").value
		
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "statusPartie",
				nom : nom,
				posx : x,
				posy : y
			}
		})
		.done(response => {
			//response = JSON.parse(response);
			console.log(response);
			if(response.etatPartie == "EN_COURS"){
				setTimeout(statusPartie,2000)
			}
			else{
				in_partie = false;
			}
		});
	}
	
}

function frapperCoup(){
	let nom = document.getElementById("nom").value
	let x = document.getElementById("posx").value
	let y = document.getElementById("posy").value
	let force = document.getElementById("force").value
	let adress = getAdress()
	
	$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "frapperCoup",
				nom : nom,
				posx : x,
				posy : y,
				force : force
			}
		})
		.done(response => {
			//response = JSON.parse(response);
			console.log(response);
		});
	
}

function finPartie(){
	let nom = document.getElementById("nom").value
	let pointGagne = Math.round(Math.random()*40)
	let pojntPerd = Math.round(Math.random()*40)
	let adress = getAdress()
	
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "finPartie",
				gagnant : nom,
				pointsPerdant : pojntPerd,
				pointsGagnant : pojntPerd
			}
		})
		.done(response => {
			//response = JSON.parse(response);
			console.log(response);
			in_partie = false;
		});
}

function getAdress(){
	let ip = document.getElementById("ip").value
	let port = "50048"
	let adress  = "http:\\\\"+ip+":"+port
	return adress
}