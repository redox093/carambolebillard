<?php
    session_start();
    
    abstract class CommonAction {

        protected static $VISIBILITY_ADMIN = 3;
		protected static $VISIBILITY_MODERATEUR = 2;
		protected static $VISIBILITY_MEMBER = 1;
        protected static $VISIBILITY_PUBLIC = 0;
        
        private $pageVisibility = null;

        public function __construct($pageVisibility) {
            $this->pageVisibility = $pageVisibility;
        }

        public function execute() {
            $this->executeAction();
        }

        abstract protected function executeAction();

        public function isLoggedIn() {
			return $_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC;
		}

        public function getUserName() {
            return $this->isLoggedIn() ? $_SESSION["username"] : "invité";
        }

        public function getCurrentPage() {
			$currentPageURL = 'http://';
			if ($_SERVER["SERVER_PORT"] != "80") {
                // ou juste cette ligne sans condition fonctionne aussi
				$currenPageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			} else {
				$currentPageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			}
			return $currentPageURL;
		}

        protected function callAPI($service, $url, array $data) {

            $options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
                    'content' => http_build_query($data)
				)
            );
            
            $context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);

            if (strpos($result, "<br") !== false) {
				var_dump($result);
				exit;
			}
			
		    return json_decode($result);
        }

    }