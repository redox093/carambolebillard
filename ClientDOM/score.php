<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <title>Carambole - Score</title>
        <link rel="icon" href="images/ballsicon.png" type="image/x-icon" />
		<script src="js/jquery.js" charset="utf-8"></script>
		<script src="js/javascript.js" charset="utf-8"></script>
		<script src="js/score.js" charset="utf-8"></script>
        <link rel="stylesheet" href="css/common.css">
		<link rel="stylesheet" href="css/game.css">
		<link rel="stylesheet" href="css/score.css">
	</head>
	<div class="back_btn" onclick="window.location = 'lobby.php'"><a href="javascript:void(0)">retour</a></div>
	<body>
		<header>
			<div class="formulaire">
				<input class="barre" type="text" value="Rechercher..."/>
				<input class="boutonRecherche" type="button" value="Valider"/>
			</div>
			<input class="trie" type="checkbox" value="Libre"/>Libre
			<input class="trie" type="checkbox" value="1 Bande"/>1 Bande
			<input class="trie" type="checkbox" value="3 Bande"/>3 Bande
			<input class="trie" type="checkbox" value="Tous" checked/>Tous
		</header>

		<div class="listeScroll">
		</div>
		<footer>We have the best of the best players here!</footer>
	</body>

</html>