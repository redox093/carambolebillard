let tick = 0;
let attente = false;
let spin = 12;

let ambiance = new Audio('./music/bgMenu.ogg');

window.onload = function () {

	document.getElementById("person-name").innerHTML = localStorage["username"];
	populateProfile();
	ambiance.play();
	ambiance.loop = true;

	// choix des parties
	document.getElementById("libre").addEventListener('click', function(event) {
		document.getElementById("titrePartie").innerHTML = "Libre";
		attente = true;
		showPanel("loading");
		creerPartie("LIBRE");
	});

	document.getElementById("une-bande").addEventListener('click', function(event) {
		document.getElementById("titrePartie").innerHTML = "Une Bande";
		attente = true;
		showPanel("loading");
		creerPartie("UN_BOND");
	});

	document.getElementById("trois-bandes").addEventListener('click', function(event) {
		document.getElementById("titrePartie").innerHTML = "Trois Bandes";
		attente = true;
		showPanel("loading");
		creerPartie("TROIS_BONDS");
	});

	document.getElementById("leaveGame").addEventListener('click', function(event) {
		attente = false;
		quitterPartie();
	});

	mainTick();

}

function mainTick() {
	
	tick++

	window.requestAnimationFrame(mainTick);
	document.getElementById("loading-point").style.left = -(36 * Math.sin(tick/spin + 60) - 27) + "px";
	document.getElementById("loading-point").style.top = 36 * Math.cos(tick/spin + 60) + 27 + "px";
	
}

function creerPartie(type){
	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "creerPartie",
			nom : nom,
			typePartie : type
		}
	})
	.done(response => {
		//response = JSON.parse(response);
        console.log(response);
        
        console.log("Creation de la partie");
		if(response.message == "ATTENTE_ADVERSAIRE"){
			setTimeout(statusDemande, 2000)
		}
		else if(response.message == "TROUVEE_ADVERSAIRE"){
            in_partie = true;
            console.log("Creation partie");
			window.location.href = 'game.php';
			//setTimeout(statusPartie, 2000)
		}
	});
}

function statusDemande() {
	let nom = localStorage["username"];
	let adress = getAdress()
	if (attente) {
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "statusDemande",
				nom : nom
			}
		})
		.done(response => {
			//response = JSON.parse(response);
			console.log(response);
			
			console.log("Statut de la DEMANDE");

			if (response.typePartie == "UN_BOND") {
				localStorage["typePartie"] = "UN_BOND";
			}
			else if (response.typePartie == "LIBRE") {
				localStorage["typePartie"] = "LIBRE";
			}
			else if (response.typePartie == "TROIS_BONDS") {
				localStorage["typePartie"] = "TROIS_BONDS";
			}

			if(response.message == "ATTENTE_ADVERSAIRE"){
				setTimeout(statusDemande, 2000)
			}
			else if(response.message == "TROUVEE_ADVERSAIRE"){
				in_partie = true;
				console.log("Statut de la demande");
				window.location.href = 'game.php';
				//setTimeout(statusPartie, 2000)
			}
		});
	}
}