let spriteList = [];
let playerPlaying = "";
let forceCoup = 0;
let playerBallX = 0;
let playerBallY = 0;
let distanceX = 0;
let distancePreX = [];
let joueur1 = false, joueur2 = false;
let mousedownID = -1;
let unMur = false, troisMurs = false;
let firstHit = false, secondHit = false, murCount = 0;

let doneInit = false

let ambiance = new Audio('./music/bgGame.ogg');

let ballknock1 = new Audio('./sound/ballknock1.mp3');
let bump = new Audio('./sound/bump.mp3');

window.onload = function() {

	document.getElementById("person-name").innerHTML = localStorage["username"];
	document.getElementById("player1-name").innerHTML = localStorage["username"];

	in_partie = true;
	statusPartie();
	ambiance.play();
	ambiance.loop = true;

}

function init(){
	
	populateProfile();
	
	spriteList.push(new Queue());
	spriteList.push(new WhiteBall());
	spriteList.push(new RedBall());
	spriteList.push(new YellowBall());

	document.getElementById("plattern").onmousemove = (event) => {

		if (playerPlaying == localStorage["username"]) {

			document.querySelector(".queue").style.display = "block"

			findPlayerBallXandY();

			document.getElementById("clientx").textContent = event.clientX - document.getElementById("plattern").offsetLeft;;
			document.getElementById("clienty").textContent = event.clientY - document.getElementById("plattern").offsetTop;

			document.getElementById("ballx").textContent = playerBallX;
			document.getElementById("bally").textContent = playerBallY;

			document.querySelector(".queue").style.left = -(36 * Math.sin(event.clientX) - 27) + "px";
			document.querySelector(".queue").style.top = (36 * Math.cos(event.clientX) + 27) + "px";

			for (let i = 0; i < spriteList.length; i++) {

				if (spriteList[i] instanceof Queue) {

					document.querySelector(".queue").style.transformOrigin="0px 50%";

					spriteList[i].x = playerBallX;
					spriteList[i].y = playerBallY;

					spriteList[i].arad = Math.atan2((playerBallY - (event.clientY - document.getElementById("plattern").offsetTop)), (playerBallX - (event.clientX - document.getElementById("plattern").offsetLeft)));
					
				}
			}

			// Décider la force du coup
			document.addEventListener("mousedown", mousedown);
			document.addEventListener("mouseup", mouseup);

		}
		else document.querySelector(".queue").style.display = "none";
	}

	generalTick();

}

function stopHit(event){
		// action avec force du coup
	if(forceCoup > 0){
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof Queue) {
				spriteList[i].dst = 50;
			}
		}
	}
}

function mousedown(event) {
	mousedownID = setInterval(whilemousedown, 20);
}

function mouseup(event) {
	// action avec force du coup
	
	for (let i = 0; i < spriteList.length; i++) {
		if (spriteList[i] instanceof Queue) {
			spriteList[i].dst = 50;
		}
	// call ajax frapperCoup() avec infos du coup -> force, nom joueur, posx, posy, etc
		if (forceCoup > 0) frapperCoup();
		
			clearInterval(mousedownID);
			forceCoup = 0;
		
	}
}

function whilemousedown() {
	if (forceCoup < 100 ) { 
		forceCoup++;
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof Queue) {
				spriteList[i].dst++;
			}
		}
	}
}

function findPlayerBallXandY() {
	if (joueur1) {
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof WhiteBall) {
				playerBallX = spriteList[i].x;
				playerBallY = spriteList[i].y;
			}
		}
	}
	else if (joueur2) {
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof YellowBall) {
				playerBallX = spriteList[i].x;
				playerBallY = spriteList[i].y;
			}
		}
	}
}

// Animer les sprites
function generalTick()  {
	
	for (let i = 0; i < spriteList.length; i++) {
		let alive = spriteList[i].tick();
	
		if (!alive) {
			spriteList.splice(i, 1); 
			i--;
		}
	}

	// Change l'affichage de l'écran
	window.requestAnimationFrame(generalTick);

}

function verifWalls() {
	if (localStorage["typePartie"] == "UN_BOND") {
		if (firstHit && !secondHit) {
			murCount++;
			if (murCount == 1) {
				unMur = true;
			}
			else unMur = false;
		}
	}
	else if (localStorage["typePartie"] == "TROIS_BONDS") {
		if (firstHit && !secondHit) {
			murCount++;
			if (murCount == 3) {
				troisMurs = true;
			}
			else troisMurs = false;
		}
	}
}

function verifBalls() {
	if (joueur1) {
		if (playerPlaying == localStorage["username"]) { // playing
			if (localStorage["typePartie"] == "LIBRE") {
				if (spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall) {
					firstHit ? secondHit = true : firstHit = true;
				}
			}
			else if (localStorage["typePartie"] == "UN_BOND") {
				if ((spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (unMur) secondHit = true;
				}
			}
			else if (localStorage["typePartie"] == "TROIS_BONDS") {
				if ((spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (troisMur) secondHit = true;
				}
			}
		}
		else { // not playing
			if (localStorage["typePartie"] == "LIBRE") {
				if (spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhitewBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall) {
					firstHit ? secondHit = true : firstHit = true;
				}
			}
			else if (localStorage["typePartie"] == "UN_BOND") {
				if ((spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhiteBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (unMur) secondHit = true;
				}
			}
			else if (localStorage["typePartie"] == "TROIS_BONDS") {
				if ((spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhiteBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (troisMur) secondHit = true;
				}
			}
		}
	}
	else if (joueur2) {
		if (playerPlaying == localStorage["username"]) { // playing
			if (localStorage["typePartie"] == "LIBRE") {
				if (spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhitewBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall) {
					firstHit ? secondHit = true : firstHit = true;
				}
			}
			else if (localStorage["typePartie"] == "UN_BOND") {
				if ((spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhiteBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (unMur) secondHit = true;
				}
			}
			else if (localStorage["typePartie"] == "TROIS_BONDS") {
				if ((spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhiteBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (troisMur) secondHit = true;
				}
			}
		}
		else { // not playing
			if (localStorage["typePartie"] == "LIBRE") {
				if (spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall) {
					firstHit ? secondHit = true : firstHit = true;
				}
			}
			else if (localStorage["typePartie"] == "UN_BOND") {
				if ((spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (unMur) secondHit = true;
				}
			}
			else if (localStorage["typePartie"] == "TROIS_BONDS") {
				if ((spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (troisMur) secondHit = true;
				}
			}
		}
	}
}

function statusDemande(){
	let nom = localStorage["username"]
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "statusDemande",
			nom : nom
		}
	})
	.done(response => {
		console.log(response);
		if(response.message == "ATTENTE_ADVERSAIRE"){
			setTimeout(statusDemande, 2000)
		}
		else if(response.message == "TROUVEE_ADVERSAIRE"){
			in_partie = true;
			setTimeout(statusPartie, 2000)
		}
	});
}

function statusPartie(){
	if(in_partie){
		let nom = localStorage["username"]
		let adress = getAdress()

		let angle = 0;
		
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof Queue) {
				angle = spriteList[i].arad
				
			}
		}
		
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "statusPartie",
				nom : nom,
				angle : angle,
				force : forceCoup
			}
		})
		.done(response => {
            console.log("Statut de la partie");
			console.log(response);

			if (response.joueur1.nom == localStorage["username"]) {
				joueur1 = true;
				joueur2 = false;
			}
			else if (response.joueur2.nom == localStorage["username"]) {
				joueur2 = true;
				joueur1 = false;
			}

			findPlayerBallXandY()
			
			document.getElementById("player1-name").innerHTML = response.joueur1.nom;
			document.getElementById("player2-name").innerHTML = response.joueur2.nom;
			
			if(!doneInit){
				init();
				doneInit = true;
				playerPlaying =  response.joueur1.nom;
			}

			if (response.etatPartie == "EN_COURS") {
				if (response.dernierCoup != "--") {
							
					if (response.dernierCoup.joueur == response.joueur2.nom) {
						playerPlaying = response.joueur1.nom;
					}
					else if (response.dernierCoup.joueur == response.joueur1.nom) {
						playerPlaying = response.joueur2.nom;
					}
					
					simuleCoup(response.dernierCoup.joueur,parseFloat(response.dernierCoup.angle),parseInt(response.dernierCoup.force))
				}
				
			}
			else {
				in_partie = false;
				quitterPartie();
			}
			setTimeout(statusPartie, 2000);
		});
	}
	
}

function frapperCoup(){

	let nom = localStorage["username"];
	let angle;
	document.querySelector(".queue").style.display = "none";

	for (let i = 0; i < spriteList.length; i++) {
		if (spriteList[i] instanceof Queue) {
			angle = spriteList[i].arad
		}
	}

	let force = forceCoup;
	let adress = getAdress()
	
	$.ajax({

			type : "GET",
			url : adress,
			data : {
				action : "frapperCoup",
				nom : nom,
				angle : angle,
				force : force
			}
		})
		.done(response => {
			console.log(response);
		});
	
	
}

function finPartie(){
	let nom = localStorage["username"];
	let pointGagne = Math.round(Math.random()*40)
	let pointPerd = Math.round(Math.random()*40)
	let adress = getAdress()
	
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "finPartie",
				gagnant : nom,
				pointsPerdant : pointPerd,
				pointsGagnant : pointGagne
			}
		})
		.done(response => {
			console.log(response);
			in_partie = false;
		});
}

function simuleCoup(nom,angle,force){
	let j1 = joueur1;
	if(nom != localStorage["username"]){
		j1 = !joueur1
	}
	
	for (let i = 0; i < spriteList.length; i++) {
		if (spriteList[i] instanceof Ball) {
			if(j1 && spriteList[i] instanceof WhiteBall){
				spriteList[i].frappe(angle,force)
				break;
			}
			else if(!j1 && spriteList[i] instanceof YellowBall){
				spriteList[i].frappe(angle,force)
				break;
			}
		}
	}
}

