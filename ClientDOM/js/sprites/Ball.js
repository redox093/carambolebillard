class Ball{
	constructor(x,y,image,nodeClass){
		this.x = x;
        this.initialX = this.x;
        this.y = y;
        this.width = 50;
        this.height = 50;
        this.image = new Image()
        this.image.src = image

        this.node = document.createElement("div");
        this.node.className = nodeClass;
        this.node.style.top = (this.y - this.width / 2) + "px";
        this.node.style.left = (this.x - this.height / 2) + "px";
		
		document.getElementById("plattern").appendChild(this.node);
		
		this.speed = 0;
		this.angle = 0;
	}
	
	tick(){
		
		if(this.speed > 0){
			let speedX = this.speed*Math.cos(this.angle)
			let speedY = this.speed*Math.sin(this.angle)
			
			this.speed -= 0.25;
			
			let w1 = 75 - this.width/2
			let h1 = 75 - this.width/2
			let w2 = 1130 + this.width/2
			let h2 = 570 + this.width/2
			
			this.x += speedX
			this.y += speedY
			
			if(this.x+speedX < w1 || this.x+speedX > w2){
				if(this.x + speedX < w1){
					this.x = w1
				}
				else if(this.x+speedX > w2){
					this.x = w2
				}
				
				speedX = -speedX
			}
			
			if(this.y+speedY < h1 || this.y+speedY > h2){
				if(this.y + speedY < h1){
					this.y = h1
				}
				else if(this.y+speedY > h2){
					this.y = h2
				}
				
				speedY = -speedY

			}
			
			for(let i = 0; i < spriteList.length; i++){
				if(spriteList[i] instanceof Ball){
					const ball = spriteList[i]
					
					let dx = ball.x - this.x
					let dy = ball.y - this.y
					
					let bx = this.speed*Math.cos(this.angle)
					let by = this.speed*Math.sin(this.angle)
					let br = Math.sqrt(Math.pow(bx-dx,2)+Math.pow(by-dy,2))
					if(Math.abs(br) <= this.width){
						let aa = Math.atan2(by-dy,bx-dx)
						this.angle = Math.atan2(by-dy,bx-dx);
						spriteList[i].angle = Math.atan2(dy-by,dx-bx);
						spriteList[i].speed = this.speed
					}
				}
			}
			
			this.angle = Math.atan2(speedY,speedX)
			
			this.node.style.top = (this.y - this.width / 2) + "px";
			this.node.style.left = (this.x - this.height / 2) + "px";
			
		}
		if(this.speed < 0){
			this.speed = 0;
		}

		return true
	}
	
	frappe(angle,force){
		this.angle = angle
		this.speed = force
	}
}