let in_partie = false;

function saveUsername() {
    localStorage["username"] = $("#usrName").val();
    localStorage["serverIP"] = $("#svrName").val();
}


function login(){
	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "connexion",
			nom : nom
		}
	})
	.done(response => {
        
        if (response.nom == localStorage["username"] || response.erreur == "USER_ALREADY_CONNECTED") {
            window.location = 'lobby.php';
        }

		console.log(response);
	});
}

function logout(){
	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "disconect",
			nom : nom
		}
	})
	.done(response => {
        console.log(response);
        
        if (response.message == "LOGOUT_SUCCESS" || response.erreur == "USER_NOT_FOUND") {
            window.location.href = 'index.php';
        }

		in_partie = false;
	});
}

function afficherScore(){

    let adress = getAdress();

    $.ajax({
        type : "GET",
        url : adress,
        data : {
            action : "statJoueur",
        }
    })
    .done(response => {
		console.log(response)
		if(response.joueurs != null){
			listeJoueur = response.joueurs
			listerJoueurs()
		}
	})
}

function quitterPartie(){

	let nom = localStorage["username"];
	let adress = getAdress();
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "leaving",
			nom : nom,
		}
	})
	.done(response => {
		window.location.href = "lobby.php";
		console.log(response);
	});

}

function getJoueur() {
	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "statJoueur",
			nom : nom,
		}
	})
	.done(response => {
		
		console.log(response);

		if (response.nom == localStorage["username"]) {
			localStorage["victoires"] = response.nbVictoire;
			localStorage["defaites"] = response.nbDefaite;
			localStorage["niveau"] = response.niveau;
		}
		
		if (localStorage["username"] != null) {
			document.getElementById("profile-name").innerHTML = localStorage["username"];
		}

		if (localStorage["niveau"] != null) {
			document.getElementById("niveau").innerHTML = "Niveau " + localStorage["niveau"];
		}

		if (localStorage["victoires"] != null) {
			document.getElementById("nbVictoires").innerHTML = localStorage["victoires"] + " Victoires";
		}

		if (localStorage["defaites"] != null) {
			document.getElementById("nbDefaites").innerHTML = localStorage["defaites"] + " Défaites";
		}
		console.log("profile populated")

	});
}

function getAdress(){
    let ip = localStorage["serverIP"];
    let port = "50048"
    let adress  = "http:\\\\" + ip + ":" + port
    return adress  
}

function populateProfile() {
	getJoueur();
}

function hidePanel(pan) {
	document.getElementById("panel-" + pan).style.bottom = "-800px";
    document.getElementById("bg-panel").style.display = "none";
    document.getElementById("panel-" + pan).style.opacity = "1";
}

function showPanel(pan){
	document.getElementById("panel-" + pan).style.display = "block";
	document.getElementById("panel-" + pan).style.bottom = "350px";
    document.getElementById("bg-panel").style.display = "block";
    document.getElementById("panel-" + pan).style.opacity = "1";
}