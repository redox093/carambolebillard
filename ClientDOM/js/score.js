let listeJoueur = []

window.onload = function (){
	afficherScore();
	
}

function listerJoueurs(){
	let conteneur = document.querySelector(".listeScroll")
	
	for(let i = 0; i < listeJoueur.length; i++){
		const element = listeJoueur[i]
		
		let ligne = document.createElement("div")
		
		let items = document.createElement("div")
		items.className = "ligneScore"
		
		let node1 = document.createElement("div")
		node1.style.textDecoration = "underline"
		let text1 = document.createTextNode(element.nom)
		node1.appendChild(text1)
		ligne.appendChild(node1)
		
		let node2 = document.createElement("div")
		node2.className = "ref"
		let text2 = document.createTextNode("Niveau: "+element.niveau)
		node2.appendChild(text2)
		items.appendChild(node2)
		
		let node3 = document.createElement("div")
		node3.className = "ref"
		let text3 = document.createTextNode("Victoires: "+element.nbVictoire)
		node3.appendChild(text3)
		items.appendChild(node3)
		
		let node4 = document.createElement("div")
		node4.className = "ref"
		let text4 = document.createTextNode("defaites: "+element.nbDefaite)
		node4.appendChild(text4)
		items.appendChild(node4)
		
		ligne.appendChild(items)
		
		conteneur.appendChild(ligne)
	}
	
}