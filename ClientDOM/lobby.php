<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <title>Carambole - Lobby</title>
        <link rel="icon" href="images/ballsicon.png" type="image/x-icon" />
        <script src="js/jquery.js" charset="utf-8"></script>
        <script src="js/javascript.js" charset="utf-8"></script>
        <script src="js/lobby.js" charset="utf-8"></script>
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/lobby.css">
    </head>

    <body>
        <div id="bg-panel"></div>
        <main>
            <nav>
                <p id="person-name"></p>
                <ul id="user-actions-nav">
                    <li id="profil"><a href="javascript:showPanel('profile')">Profil</a></li>
                    <li><a href="javascript:logout()">Déconnexion</a></li>
                </ul>
            </nav>

            <header><img src="images/Title2.png"/></header>

            

            <section id="gametypes">
                <h1>Choisissez un mode de jeu</h1>

                <ul>
                    <li id="libre" class="type">Libre<img src="images/bg-sketch-blue-vert.png"/></div>
                    <li id="une-bande" class="type">1 bande<img src="images/bg-sketch-blue-vert.png"/></div>
                    <li id="trois-bandes" class="type">3 bandes<img src="images/bg-sketch-blue-vert.png"/></div>
                </ul>

            </section>

            <section id=misc>
                <div id="show-scores" onclick="window.location.replace('score.php')">Table des scores</div>
            </section>
        </main>

            <div id="panel-loading" class="panel">
                <p id="titrePartie">Libre</p>
                <p>Recherche d'un adversaire sur le serveur...</p>
                <div id="loading-clock"><div id="loading-point"></div></div>
                <p><br/></p>
                <p id="leaveGame"><a href="javascript:hidePanel('loading')">Annuler</a></p>
            </div>

            <div id="panel-profile" class="panel">
                    <div id="profile-pic"></div>
                    <h1 id="profile-name">Some user</h1>
                    <p id="niveau">0</p>
                    <p><br/></p>
                    <p id="nbVictoires">0 victoires</p>
                    <p id="nbDefaites">0 défaites</p>
                    <p><br/></p>
                    <p><a href="javascript:hidePanel('profile')">Fermer</a></p>
            </div>

            

        <!-- </main> -->

        

    </body>

</html>