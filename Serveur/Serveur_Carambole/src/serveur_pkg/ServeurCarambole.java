package serveur_pkg;

import java.net.*;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;

import carambole.Coup;
import carambole.DemandePartie;
import carambole.InfoJoueur;
import carambole.Joueur;
import carambole.Partie;
import dao_oracle.DAO;

import java.io.*;


public class ServeurCarambole {
	private ServerSocket serveur;
	private static final int port = 50048;
	private Vector<Thread> thread;
	private static final long timeout = 60000;
	
	private HashMap<String, Joueur> joueursActif;
	private Vector<DemandePartie> demandePartie;
	private Vector<Partie> partieEnCours;
	
	private boolean update;
	
	public ServeurCarambole(){
		joueursActif = new HashMap<String,Joueur>();
		demandePartie = new Vector<DemandePartie>();
		partieEnCours = new Vector<Partie>();
		thread = new Vector<Thread>();
		
		try{
			serveur = new ServerSocket(port,20,InetAddress.getLocalHost());
			System.out.println(serveur);
		
			
			demarerEcoute();
			
			//Fermeture serveur
			//fermerServeur();
			
		}
		catch(Exception e){
			
		}
	}
	
	public boolean running(){
		if(serveur != null)
			return !serveur.isClosed();
		
		else{
			return true;
		}
	}
	
	public void update(){
		if(!update){
			update = true;
			long timeNow= System.currentTimeMillis();
			Vector<String> disconect = new Vector<String>();
			Vector<DemandePartie> buff1 = new Vector<DemandePartie>();
			Vector<Partie> buff2 = new Vector<Partie>();
			Vector<Thread> buff3 = new Vector<Thread>();
			
			for(String key : joueursActif.keySet()){
				Joueur j = joueursActif.get(key);
				if((timeNow - j.getLastCall()) > timeout ){
					disconect.add(key);
					
					for(DemandePartie d :demandePartie){
						if(d.getJoueur() == j){
							buff1.add(d);
						}
					}
					
					for(Partie p : partieEnCours){
						if(p.contienJoueur(j)){
							p.joueurDeconnecte(j);
						}
					}
				}
			}
			
			for(DemandePartie d : demandePartie){
				if((timeNow - d.getTempsDemande()) > timeout*3){
					if(!buff1.contains(d)){
						buff1.add(d);
					}
				}
			}
			
			for(Partie p : partieEnCours){
				boolean ok = false;
				for(InfoJoueur j :  p.getJoueurs()){
					if(joueursActif.containsValue(j.getJoueur())){
						ok = true;
						break;
					}
					
				}
				if(!ok){
					buff2.add(p);
				}
			}
			
			for(Thread t : thread){
				if(t.isInterrupted()){
					buff3.add(t);
				}
			}
			
			for(Thread t : buff3){
				if(thread.contains(t)){
					thread.remove(t);
				}
			}
			
			for(String key : disconect){
				if(joueursActif.containsKey(key)){
					joueursActif.remove(key);
				}
			}
			
			for(DemandePartie d : buff1){
				if(demandePartie.contains(d)){
					demandePartie.remove(d);
				}
			}
			
			for(Partie p : buff2){
				if(partieEnCours.contains(p)){
					partieEnCours.remove(p);
				}
			}
			update = false;
		}
	}
	
	public void fermerServeur(){
		try {
			if(thread != null){
				for(Thread t : thread){
					t.interrupt();
				}
				
				DAO.fermerConnexion();

				serveur.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void demarerEcoute(){
		Thread threadEcoute = new Thread(new EcouteThread());
		threadEcoute.start();
		thread.add(threadEcoute);
	
	}
	
	private String executeDemande(HashMap<String,String> variables){
		HashMap<String,String> rep = new HashMap<String,String>();
		while(update);
		update = true;
		String action = "";
		//Code pour changer reponse dependant de la demande
		if(variables.containsKey("action")){
			action = variables.get("action");
			
			if(action.equals("connexion")){
				if(variables.containsKey("nom")){
					String nom = variables.get("nom");
					
					if(joueursActif.containsKey(nom)){
						rep.put("erreur", "USER_ALREADY_CONNECTED");
						// Erreur!
					}
					else{
					
						//Cherche joueur dans BD
						Joueur j = DAO.readJoueur(nom);
						
						joueursActif.put(nom, j);
						
						rep.put("nom", j.getNom());
						rep.put("niveau", Integer.toString(j.getNiveau()));
					}
				}
				else{
					rep.put("erreur", "MISSING_PARAMETER");
					//Erreur!
				}
			}
			else if(action.equals("statusPartie")){
				if(variables.containsKey("nom") && variables.containsKey("angle") && variables.containsKey("force")){
					try{
						String nom = variables.get("nom");
						
						double angle = Double.parseDouble(variables.get("angle"));
						double force = Double.parseDouble(variables.get("force"));
						
						Joueur j = null;
						Partie partie = null;
						if(joueursActif.containsKey(nom)){
							joueursActif.get(nom).call();
							j = joueursActif.get(nom);
							
							for(Partie p : partieEnCours){
								if(p.contienJoueur(j)){
									partie = p;
									break;
								}
							}
							
							if(partie != null){
								partie.updateJoueur(nom,angle,force);
								
								InfoJoueur j1 = partie.getJoueur(0);
								InfoJoueur j2 = partie.getJoueur(1);
								
								rep.put("etatPartie",partie.getEtatPartie());
								rep.put("joueur1", 
										"{\"nom\":\""+j1.getJoueur().getNom()+"\","
										+"\"force\":\""+j1.getForce()+"\","
										+"\"angle\":\""+j1.getAngle()+"\""
										+"}");
								rep.put("joueur2", 
										"{\"nom\":\""+j2.getJoueur().getNom()+"\","
										+"\"force\":\""+j2.getForce()+"\","
										+"\"angle\":\""+j2.getAngle()+"\""
										+"}");
								
								/*
								rep.put("joueur1", j1.getJoueur().getNom());
								rep.put("joueur1_pos", "{\"x\":\""+j1.getPos().x+"\",\"y\":\""+j1.getPos().y+"\"}");
								rep.put("joueur2", j2.getJoueur().getNom());
								rep.put("joueur2_pos", "{\"x\":\""+j2.getPos().x+"\",\"y\":\""+j2.getPos().y+"\"}");
								*/
								String dernierCoup = "";
								Coup coup = partie.getDernierCoup();
								
								if(coup == null || !coup.not_ack(j)){
									dernierCoup = "--";
								}
								else{
									dernierCoup = "{"
											+"\"angle\":\""+coup.getAngle()+"\","
											+"\"force\":\""+coup.getForce()+"\","
											+"\"joueur\":\""+coup.getJoueur().getNom()+"\""
											+"}";
									
									partie.getDernierCoup().ack_coup(j);
									if(partie.getDernierCoup().all_ack()){
										partie.removeDernierCoup();
									}
									
								}
								rep.put("dernierCoup", dernierCoup);
								


								
							}
							else{
								rep.put("erreur", "GAME_NOT_FOUND");
								//Erreur!
							}
						}
						else{
							rep.put("erreur", "USER_NOT_FOUND");
							//Erreur!
						}
					}
					catch(Exception e){
						rep.put("erreur", "WRONG_PARAMETER");
						e.printStackTrace();
						//Erreur!
					}
					
					
				}
				else{
					rep.put("erreur", "MISSING_PARAMETER");
					//Erreur!
				}
			}
			else if(action.contains("statusDemande")){
				if(variables.containsKey("nom")){
					String nom = variables.get("nom");
					Joueur j = null;
					Partie partie = null;
					DemandePartie demande = null;
					
					if(joueursActif.containsKey(nom)){
						joueursActif.get(nom).call();
						j = joueursActif.get(nom);
						
						for(DemandePartie d : demandePartie){
							if(d.getJoueur() == j){
								demande = d;
								break;
							}
						}
						
						if(demande == null){
							//Cherche si partie cr��
							for(Partie p : partieEnCours){
								if(p.contienJoueur(j)){
									partie = p;
									break;
								}
							}
							
							if(partie != null){
								//Partie d�marr�
								InfoJoueur adversaire = null;
								for(InfoJoueur ij : partie.getJoueurs()){
									if(ij.getJoueur() != j){
										adversaire = ij;
									}
								}
								
								rep.put("message", "TROUVEE_ADVERSAIRE");
								rep.put("adversaire",adversaire.getJoueur().getNom());
								rep.put("pointGagne", Integer.toString(partie.getNbPointsGagnant()));
								rep.put("typePartie",partie.getTypePartie());
							}
							else{
								rep.put("erreur", "GAME_REQUEST_NOT_FOUND");
								//Erreur!
							}
						}
						else{
							//Toujours en attente
							rep.put("message", "ATTENTE_ADVERSAIRE");
							rep.put("typePartie", demande.getTypePartie());
						}
					}
					else{
						rep.put("erreur", "USER_NOT_FOUND");
						//Erreur!
					}
				}
				else{
					rep.put("erreur", "MISSING_PARAMETER");
					//Erreur!
				}
					
			}
			else if(action.contains("disconect")){
				if(variables.containsKey("nom")){
					String nom = variables.get("nom");
					
					if(joueursActif.containsKey(nom)){
						Joueur j = joueursActif.get(nom);
						
						joueursActif.remove(nom);
						
						Vector<DemandePartie> buff = new Vector<DemandePartie>();
						
						for(DemandePartie d :demandePartie){
							if(d.getJoueur() == j){
								buff.add(d);
							}
						}
						
						for(Partie p : partieEnCours){
							if(p.contienJoueur(j)){
								p.joueurDeconnecte(j);
								
							}
						}
						
						for(DemandePartie d : buff){
							if(demandePartie.contains(d)){
								demandePartie.remove(d);
							}
						}
						
						rep.put("message", "LOGOUT_SUCCESS");
						
					}
					else{
						rep.put("erreur", "USER_NOT_FOUND");
						//Erreur!
					}
				}
				else{
					rep.put("erreur", "MISSING_PARAMETER");
					//Erreur!
				}
			}
			else if(action.equals("creerPartie")){
				if(variables.containsKey("nom") && variables.containsKey("typePartie")){
					String nom = variables.get("nom");
					String typePartie = variables.get("typePartie");
					
					boolean ok = false;
					for (String type : DemandePartie.ChoixPartie){
						if(type.equals(typePartie)){
							ok = true;
							break;
						}
					}
					
					if(ok){	
						if(joueursActif.containsKey(nom)){
							joueursActif.get(nom).call();
							Joueur j = joueursActif.get(nom);
							
							DemandePartie trouverMatch = null;
							boolean erreur = false;
							for(DemandePartie d : demandePartie){
								if(d.getJoueur() == j){
									//Erreur!
									rep.put("erreur", "ALREADY_WAITING_GAME");
									rep.put("typePartie", d.getTypePartie());
									erreur = true;
									break;
								}
								else if(d.getTypePartie().equals(typePartie)){
									trouverMatch = d;
									break;
								}
							}
							if(!erreur){
								if(trouverMatch == null){
									//Creer demande�
									demandePartie.add(new DemandePartie(j,typePartie));
									rep.put("typePartie", typePartie);
									rep.put("message", "ATTENTE_ADVERSAIRE");
								}
								else{
									//Creer Partie
									demandePartie.remove(trouverMatch);
									Partie p = new Partie(new Joueur[]{trouverMatch.getJoueur(),j},typePartie);
									partieEnCours.add(p);
									
									rep.put("message", "TROUVEE_ADVERSAIRE");
									rep.put("adversaire",trouverMatch.getJoueur().getNom());
									rep.put("pointGagne", Integer.toString(p.getNbPointsGagnant()));
									rep.put("typePartie",p.getTypePartie());
								}
							}
						}
						else{
							rep.put("erreur", "USER_NOT_FOUND");
							//Erreur!
						}
					}
					else{
						rep.put("erreur", "NOT_A_GAME_TYPE");
						//Erreur!
					}
				}
				else{
					rep.put("erreur", "MISSING_PARAMETER");
					//Erreur!
				}
			}
			else if(action.equals("finPartie")){
				if(variables.containsKey("gagnant") && variables.containsKey("pointsPerdant") && variables.containsKey("pointsGagnant")){
					String gagnant = variables.get("gagnant");
					int pointPerdant = -1;
					int pointGagnant = -1;
					Partie partie = null;
					
					try{
						pointPerdant = Integer.parseInt(variables.get("pointsPerdant"));
						pointGagnant = Integer.parseInt(variables.get("pointsGagnant"));
					
						if(pointPerdant >= 0 && pointGagnant >= 0){
							if(joueursActif.containsKey(gagnant)){
								joueursActif.get(gagnant).call();
								Joueur j = joueursActif.get(gagnant);
								
								for(Partie p : partieEnCours){
									if(p.contienJoueur(j)){
										partie = p;
										break;
									}
								}
								
								if(partie != null){
									InfoJoueur[] joueurs = partie.getJoueurs();
									
									for(InfoJoueur ij : joueurs){
										if(ij.getJoueur().getNom().equals(gagnant)){
											partie.updatePoints(ij.getJoueur().getNom(), pointGagnant);
										}
										else{
											partie.updatePoints(ij.getJoueur().getNom(), pointPerdant);
										}
									}
									partie.finPartie(j);
									
									partieEnCours.remove(partie);
									
									rep.put("message","FIN_PARTIE");
								}
								else{
									//Erreur!
									rep.put("erreur", "GAME_NOT_FOUND");
								}	
							}
							else{
								//Erreur!
								rep.put("erreur", "USER_NOT_FOUND");
							}
						}
						else{
							//Erreur!
							rep.put("erreur", "WRONG_PARAMETER");
						}
					}
					catch(Exception e){
						//Erreur!
						rep.put("erreur", "WRONG_PARAMETER");
					}
				}
				else{
					//Erreur!
					rep.put("erreur", "MISSING_PARAMETER");
				}
			}
			else if(action.equals("frapperCoup")){
				if(variables.containsKey("nom") && variables.containsKey("angle") && variables.containsKey("force")){
					try{
						String nom = variables.get("nom");
						double angle = Double.parseDouble(variables.get("angle"));
						double force = Double.parseDouble(variables.get("force"));
						
						if(joueursActif.containsKey(nom)){
							joueursActif.get(nom).call();
							Joueur joueur = joueursActif.get(nom);
							Partie partie = null;
							
							
							for(Partie p : partieEnCours){
								if(p.contienJoueur(joueur)){
									partie = p;
									break;
								}
							}
							
							if(partie != null){
								Coup c = new Coup(angle,force,joueur,partie.getJoueurs());
								
								partie.updateDernierCoup(c);
								
								rep.put("message", "HINT_SEND");
							}
							else{
								//Erreur!
								rep.put("erreur", "GAME_NOT_FOUND");
							}
						}
						else{
							//Erreur!
							rep.put("erreur", "USER_NOT_FOUND");
						}
						
					}
					catch(Exception e){
						//Erreur!
						rep.put("erreur", "WRONG_PARAMETER");
					}
				}
				else{
					//Erreur!
					rep.put("erreur", "MISSING_PARAMETER");
				}
			}
			else if(action.equals("statJoueur")){
				Vector<Joueur> joueurs = DAO.readJoueur();
				if(variables.containsKey("nom")){
					String nom = variables.get("nom");
					if(joueursActif.containsKey(nom)){
						joueursActif.get(nom).call();
						
						Joueur j = DAO.readJoueur(nom);
						
						rep.put("nom", j.getNom());
						rep.put("niveau", Integer.toString(j.getNiveau()));
						rep.put("nbDefaite",Integer.toString( j.getNb_defaite()));
						rep.put("nbVictoire", Integer.toString(j.getNb_victoire()));
					}
					else{
						//Erreur!
						rep.put("erreur", "USER_NOT_FOUND");
					}
					
				}
				else{
					if(joueurs.size() > 0){
						String list = "[";
						
						for(Joueur j : joueurs){
							String joueur = "{"
									+ "\"nom\":\""+j.getNom()+"\","
									+ "\"niveau\":\""+j.getNiveau()+"\","
									+ "\"nbDefaite\":\""+j.getNb_defaite()+"\","
									+ "\"nbVictoire\":\""+j.getNb_victoire()+"\""
									+ "},";
							
							list += joueur;
						}
						
						list = list.substring(0, list.length()-1);
						
						list += "]";
						
						rep.put("joueurs", list);
					}
					else{
						//Erreur!
						rep.put("erreur", "NO_PLAYERS");
					}
				}
			}
			else if(action.equals("leaving")){
				if(variables.containsKey("nom")){
					String nom = variables.get("nom");
					boolean ok = false;
					
					if(joueursActif.containsKey(nom)){
						joueursActif.get(nom).call();
						Joueur j = joueursActif.get(nom);
						
						Vector<DemandePartie> buff1 = new Vector<DemandePartie>();
						
						for(DemandePartie d :demandePartie){
							if(d.getJoueur() == j){
								buff1.add(d);
								ok = true;
							}
						}
						
						for(Partie p : partieEnCours){
							if(p.contienJoueur(j)){
								p.joueurDeconnecte(j);
								ok = true;
							}
						}
						
						for(DemandePartie d : buff1){
							if(demandePartie.contains(d)){
								demandePartie.remove(d);
							}
						}
						
						if(ok){
							rep.put("message", "LEAVING_SUCCESS");
						}
						else{
							//Erreur!
							rep.put("erreur", "NOTHING_TO_LEAVE_FROM");
						}
						
					}
					else{
						//Erreur!
						rep.put("erreur", "USER_NOT_FOUND");
					}
				}
				else{
					//Erreur!
					rep.put("erreur", "MISSING_PARAMETER");
				}
				
			}
			else{
				//Erreur!
				rep.put("erreur", "NOT_A_SERVICE");
			}
		}
		else{
			//Erreur!
			rep.put("erreur", "MISSING_PARAMETER");
		}
		
		update = false;
	
		String reponse = "{";
		
		for(String key : rep.keySet()){
			if(rep.get(key).contains("{")){
				reponse += "\""+key+"\":"+rep.get(key)+",";
			}
			else{
				reponse += "\""+key+"\":\""+rep.get(key)+"\",";
			}
		}
		reponse = reponse.substring(0, reponse.length()-1);
		reponse+="}";
		ServeurCarambole.this.update();
		return reponse;
	}
	
	private class EcouteThread implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!serveur.isClosed()){
				try{
					Socket client = serveur.accept();
					
					Thread t = new ProcessClientThread(client);
					t.start();
					thread.add(t);
					
				}
				catch(Exception e){
					
				}
			}
		}
	}
	
	private class ProcessClientThread extends Thread{
		
		private Socket client;
		private HashMap<String,String> variables;
		
		public ProcessClientThread(Socket client){
			this.client = client;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try{
				PrintWriter out = new PrintWriter(client.getOutputStream(), true);
				BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));
				
				String answer;
				String demande = "";
				while((answer = input.readLine()) != null && !answer.equals("")){
					//System.out.println(answer);
					if(answer.contains("GET")){
						int index = answer.indexOf("?");
						if(index >= 0){
							demande = answer.substring(index+1);
							index = demande.indexOf("HTTP");
							if(index >= 0){
								demande = demande.substring(0, index-1);
							}
						}
					}
					//demande += answer;
				}
				setHash(demande);
				String reponse =  executeDemande(variables);
				
				// entete de demande
				out.print("HTTP/1.1 200 OK\r\n" +
						"Access-Control-Allow-Origin:*\r\n"+
					    "Content-Type: application/json\r\n" +
					    "Content-Length: "+reponse.length()+"\r\n\r\n");
				
				out.print(reponse);
				out.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			finally{
				this.interrupt();
			}
		}
		
		private void setHash(String demande){
			variables = new HashMap<String,String>();
			String[] parts = demande.split("&");
			
			for(String p : parts){
				int index = p.indexOf("=");
				if(index >= 0){
					String key = p.substring(0,index);
					String val = p.substring(index+1);
					
					variables.put(key, val);
				}
			}
			
		}
		
	}
}
	
