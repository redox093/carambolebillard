package dao_oracle;

import carambole.InfoJoueur;
import carambole.Joueur;
import carambole.Partie;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Vector;


public class DAO {
	private static Connection con = null;
	
	public static void writePartie(Partie p,InfoJoueur gagnant){
		Connection con = connexionOracle();
		
		InfoJoueur perdant = null;
		
		for(InfoJoueur ij : p.getJoueurs()){
			if(ij != gagnant){
				perdant = ij;
			}
		}
		
		if(perdant != null){
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			
			String dateToStr = format.format(new Date(p.getDatePartie()));
			
			String type_partie = DAO.getTypePartie(p.getTypePartie());
			
			
			
		
			try {
				Statement stm = con.createStatement();
				
				stm.executeUpdate("INSERT INTO partie(ID_TYPE_PARTIE,ID_JOUEUR_GAGNANT,ID_JOUEUR_PERDANT,DATE_PARTIE,SCORE_GAGNANT,SCORE_PERDANT)"
						+"VALUES("
						+"(SELECT ID FROM type_partie WHERE nom_partie = '"+type_partie+"'),"
						+"(SELECT ID FROM joueur WHERE nom_joueur = '"+gagnant.getJoueur().getNom()+"'),"
						+"(SELECT ID FROM joueur WHERE nom_joueur = '"+perdant.getJoueur().getNom()+"'),"
						+"TO_DATE('YYYY-MM-DD','"+dateToStr+"'),"
						+gagnant.getPoints()+","
						+perdant.getPoints()+")");
				
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			gagnant.getJoueur().gagne();
			perdant.getJoueur().perd();
			
			DAO.updateJoueur(perdant.getJoueur());
			DAO.updateJoueur(gagnant.getJoueur());
		}
	}
	
	private static String getTypePartie(String type){
		String type_partie = "";
		
		if(type.equals("LIBRE")){
			type_partie = "libre";
		}
		else if(type.equals("UN_BOND")){
			type_partie = "une bande";
		}
		else if(type.equals("TROIS_BONDS")){
			type_partie = "trois bandes";
		}
		
		return type_partie;
	}
	
	private static String getPartieType(String type){
		String type_partie = "";
		
		if(type.equals("libre")){
			type_partie = "LIBRE";
		}
		else if(type.equals("une bande")){
			type_partie = "UN_BOND";
		}
		else if(type.equals("trois bandes")){
			type_partie = "TROIS_BONDS";
		}
		
		return type_partie;
	}
	
	public static Vector<Partie> readPartie(){
		Vector<Partie> p = new Vector<Partie>();
		
		Connection con = connexionOracle();
		try {
			Statement stm = con.createStatement();
			
			ResultSet rs = stm.executeQuery("SELECT "
					+"(SELECT nom_partie FROM type_partie WHERE id = id_type_partie) AS \"TYPE_PARTIE\","
					+"(SELECT nom_joueur FROM joueur WHERE id = id_joueur_gagnant) AS \"JOUEUR_GAGNANT\","
					+"(SELECT nom_joueur FROM joueur WHERE id = id_joueur_perdant) AS \"JOUEUR_PERDANT\","
					+"TO_CHAR(date_partie,'YYYY-MM-DD') AS \"DATE_JEU\","
					+"SCORE_GAGNANT,"
					+"SCORE_PERDANT"
					+ " FROM partie");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			while(rs.next()){
				String type_partie = DAO.getPartieType(rs.getString("TYPE_PARTIE"));
				String gagnant = rs.getString("JOUEUR_GAGNANT");
				String perdant = rs.getString("JOUEUR_PERDANT");
				int score_gagnant = rs.getInt("SCORE_GAGNANT");
				int score_perdant = rs.getInt("SCORE_PERDANT");
				
				java.util.Date d;
				long date;
				try {
					d = format.parse(rs.getString("DATE_JEU"));
					date = d.getTime();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Partie pa = new Partie(new Joueur[]{new Joueur(gagnant),new Joueur(perdant)},type_partie);
				pa.getJoueur(gagnant).setPoints(score_gagnant);
				pa.getJoueur(perdant).setPoints(score_perdant);
				p.add(pa);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return p;
	}
	
	public static Vector<Joueur> readJoueur(){
		Vector<Joueur> j = new Vector<Joueur>();
		
		Connection con = connexionOracle();
		try {
			Statement stm = con.createStatement();
			String query = "SELECT NOM_JOUEUR,NIVEAU_JOUEUR,NBR_VICTOIRE,NBR_DEFAITES,"
					+"CAST(CASE WHEN NBR_DEFAITES = 0THEN 0 ELSE (NBR_VICTOIRE/NBR_DEFAITES)END AS NUMBER) AS TAUX_VICTOIRE"
					+" FROM joueur"
					+" ORDER BY TAUX_VICTOIRE DESC,NOM_JOUEUR";
			ResultSet rs = stm.executeQuery(query);

			while(rs.next()){
				String nom = rs.getString("NOM_JOUEUR");
				int niveau = rs.getInt("NIVEAU_JOUEUR");
				int nb_victoire = rs.getInt("NBR_VICTOIRE");
				int nb_defaite = rs.getInt("NBR_DEFAITES");
				
				j.add(new Joueur(nom,niveau,nb_defaite,nb_victoire));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return j;
	}
	
	public static Joueur readJoueur(String nom){
		Joueur j = new Joueur(nom);
		
		Connection con = connexionOracle();
		try {
			Statement stm = con.createStatement();
			
			ResultSet rs = stm.executeQuery("SELECT * FROM joueur WHERE nom_joueur = '"+nom+"'");
			
			if(rs.next()){
				int niveau = rs.getInt("niveau_joueur");
				int nb_victoire = rs.getInt("NBR_VICTOIRE");
				int nb_defaite = rs.getInt("NBR_DEFAITES");
				
				j = new Joueur(nom,niveau,nb_defaite,nb_victoire);
			}
			else{
				DAO.writeJoueur(j);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return j;
	}
	
	
	public static void writeJoueur(Joueur j){
		Connection con = connexionOracle();
		try {
			Statement stm = con.createStatement();
			
			stm.executeUpdate("INSERT INTO joueur(NOM_JOUEUR,NIVEAU_JOUEUR,NBR_VICTOIRE,NBR_DEFAITES) VALUES('"
							+j.getNom()+"',"+j.getNiveau()+",0,0)");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void updateJoueur(Joueur j){
		Connection con = connexionOracle();
		try {
			Statement stm = con.createStatement();
			
			stm.executeUpdate("UPDATE joueur SET"
					+"NBR_VICTOIRE = "+j.getNb_victoire()+","
					+"NBR_DEFAITES = "+j.getNb_defaite()+","
					+"NIVEAU_JOUEUR = "+j.getNiveau()
					+"WHERE NOM_JOUEUR = '"+j.getNom()+"'");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static Connection connexionOracle(){
		if(con == null){
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				
				con=DriverManager.getConnection(  
						"jdbc:oracle:thin:@delta:1521:decinfo","e1125028","AAAaaa111");  
				
				//delta : host
				//sid : decinfo
				//port : 1521
				//connexion : eMatricule/mdp@decinfo
			}
			
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return con;
		
	}
	
	public static void fermerConnexion(){
		if(con != null){
			try {
				con.close();
				con = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
}
