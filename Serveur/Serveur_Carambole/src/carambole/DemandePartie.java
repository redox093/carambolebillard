package carambole;

public class DemandePartie {
	public static final String[] ChoixPartie = {"LIBRE","UN_BOND","TROIS_BONDS"};
	
	private Joueur joueur;
	private String typePartie;
	private long tempsDemande;
	
	public DemandePartie(Joueur joueur,String typePartie){
		this.joueur = joueur;
		this.typePartie = typePartie;
		tempsDemande = System.currentTimeMillis();
	}
	
	public Joueur getJoueur(){
		return joueur;
	}
	
	public String getTypePartie(){
		return typePartie;
	}
	
	public long getTempsDemande(){
		return tempsDemande;
	}
}
