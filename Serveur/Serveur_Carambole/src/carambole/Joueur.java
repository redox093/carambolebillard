package carambole;

public class Joueur {
	private String nom;
	private int niveau;
	private long lastCall;
	private int nb_defaite;
	private int nb_victoire;
	
	public Joueur(String nom){
		this.nom = nom;
		this.niveau = 1;
		lastCall = System.currentTimeMillis();
		nb_defaite = 0;
		nb_victoire = 0;
	}
	
	public Joueur(String nom,int niveau){
		this(nom);
		this.niveau = niveau;
		
	}
	
	public Joueur(String nom,int niveau,int nb_defaite,int nb_victoire){
		this(nom,niveau);
		
		this.nb_defaite = nb_defaite;
		this.nb_victoire = nb_victoire;
	}
	
	public String getNom(){
		return nom;
	}
	
	public int getNiveau(){
		return niveau;
	}
	
	public long getLastCall(){
		return lastCall;
	}
	
	public int getNb_defaite() {
		return nb_defaite;
	}

	public int getNb_victoire() {
		return nb_victoire;
	}

	public void call(){
		lastCall = System.currentTimeMillis();
	}
	
	public void perd(){
		this.nb_defaite++;
	}
	
	public void gagne(){
		this.nb_victoire++;
	}
}
