package carambole;

import java.awt.Point;

public class InfoJoueur {
	private Joueur joueur;
	private int points;
	private double angle;
	private double force;
	
	public InfoJoueur(Joueur joueur){
		this.joueur = joueur;
		this.points = 0;
		this.angle = 0;
		this.force = 0;
	}
	
	public InfoJoueur(Joueur joueur,int point){
		this(joueur);
		this.points = point;
	}
	
	public void setPoints(int points){
		this.points = points;
	}
	
	public Joueur getJoueur(){
		return joueur;
	}
	
	public int getPoints(){
		return points;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public double getForce() {
		return force;
	}

	public void setForce(double force) {
		this.force = force;
	}
	
	public void setPos(double angle,double force){
		this.angle = angle;
		this.force = force;
	}

}
