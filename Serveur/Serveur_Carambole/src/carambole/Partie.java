package carambole;

import dao_oracle.DAO;

public class Partie {
	
	private InfoJoueur[] joueurs;
	private String typePartie;
	private int nbPointsGagnant = 40;
	private long datePartie;
	private String etatPartie;
	private Coup dernierCoup;
	
	public Partie(Joueur[] joueurs,String typePartie){
		this.joueurs = new InfoJoueur[joueurs.length];
		
		for(int i = 0; i < joueurs.length;i++){
			this.joueurs[i] = new InfoJoueur(joueurs[i]);
		}
		
		this.typePartie = typePartie;
		datePartie = System.currentTimeMillis();
		
		this.etatPartie = "EN_COURS";
		dernierCoup = null;
	}
	
	public InfoJoueur getJoueur(int noJoueur){
		if(noJoueur >= 0 && noJoueur < joueurs.length){
			return joueurs[noJoueur];
		}
		else{
			return null;
		}
	}
	
	public InfoJoueur getJoueur(String nom){
		for(InfoJoueur ij : joueurs){
			if(ij.getJoueur().getNom().equals(nom)){
				return ij;
			}
		}
		return null;
	}
	
	public InfoJoueur[] getJoueurs(){
		return joueurs;
	}
	
	public String getTypePartie(){
		return typePartie;
	}
	
	public long getDatePartie(){
		return datePartie;
	}
	
	public int getNbPointsGagnant(){
		return nbPointsGagnant;
	}
	
	public String getEtatPartie(){
		return etatPartie;
	}
	
	public Coup getDernierCoup(){
		return dernierCoup;
	}
	
	public boolean contienJoueur(Joueur joueur){
		for(InfoJoueur j : joueurs){
			if(j.getJoueur() == joueur){
				return true;
			}
		}
		return false;
	}
	
	public void joueurDeconnecte(Joueur j){
		InfoJoueur joueur = null;
		Joueur gagnant = null;
		//Envoi message � joueur restant
		etatPartie = "PARTIE_TERMINEE";
		
		
		
	}
	
	public void finPartie(Joueur j){
		InfoJoueur joueur = null;
		
		for(InfoJoueur ij : joueurs){
			if(ij.getJoueur() == j){
				joueur = ij;
				break;
			}
		}
		
		if(joueur != null){
			//Envoi donn� � BD
			DAO.writePartie(this,joueur);
			etatPartie = "PARTIE_TERMINEE";
		}
	}
	
	public void updateJoueur(String nom,double angle,double force){
		for(InfoJoueur ij : joueurs){
			if(ij.getJoueur().getNom().equals(nom)){
				ij.setPos(angle,force);
				break;
			}
		}
	}
	
	public void updatePoints(String nom,int points){
		for(InfoJoueur ij : joueurs){
			if(ij.getJoueur().getNom().equals(nom)){
				ij.setPoints(points);
				break;
			}
		}
	}
	
	public void updateDernierCoup(Coup coup){
		for(InfoJoueur ij : joueurs){
			if(ij.getJoueur() == coup.getJoueur()){
				dernierCoup = coup;
				break;
			}
		}
	}
	
	public void removeDernierCoup(){
		dernierCoup = null;
	}
	
}
