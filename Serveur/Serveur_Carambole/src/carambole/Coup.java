package carambole;

import java.util.Vector;

public class Coup {
	private double force;
	private double angle;
	private Joueur joueur;
	private Vector<Joueur> ack_joueurs;
	
	public Coup(double angle,double force,Joueur joueur,InfoJoueur[] liste_jouuers){
		this.angle = angle;
		this.force = force;
		this.joueur = joueur;
		
		ack_joueurs = new Vector<Joueur>();
		
		for(InfoJoueur j : liste_jouuers){
			ack_joueurs.add(j.getJoueur());
		}
	}
	
	public double getAngle(){
		return angle;
	}

	public double getForce() {
		return force;
	}
	
	public Joueur getJoueur(){
		return joueur;
	}
	
	public void ack_coup(Joueur joueur){
		if(ack_joueurs.contains(joueur)){
			ack_joueurs.remove(joueur);
		}
	}
	
	public boolean all_ack(){
		if(ack_joueurs.size() == 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean not_ack(Joueur joueur){
		if(ack_joueurs.contains(joueur)){
			return true;
		}
		else{
			return false;
		}
	}

}
