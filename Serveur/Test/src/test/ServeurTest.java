package test;

import java.net.*;
import java.io.*;

public class ServeurTest {
	ServerSocket serveur;
	
	ServeurTest(){
		try {
			serveur = new ServerSocket(50048,20,InetAddress.getLocalHost());
			System.out.println(serveur);
			boolean continu = true;
			
			while(continu){
				Socket s = serveur.accept();
				
				System.out.println(s.getLocalSocketAddress()+" "+s.getRemoteSocketAddress());
				
				PrintWriter out = new PrintWriter(s.getOutputStream(), true);
				BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
				
				String answer;
				while((answer = input.readLine()) != null && !answer.equals("")){
					//System.out.println(answer);
					
				}
				
				String reponse = "{\"returnkey\" : \"returnvalue\"}";
				out.print("HTTP/1.1 200 OK\r\n" +
						"Access-Control-Allow-Origin:*\r\n"+
					    "Content-Type: application/json\r\n" +
					    "Content-Length: "+reponse.length()+"\r\n\r\n");
				out.print(reponse);
				out.close();
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ServeurTest serveur = new ServeurTest();
	}

}
