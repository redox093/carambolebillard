# -*- coding: utf-8 -*-
import cx_Oracle
import sys


def connection():
	oracle_connection = cx_Oracle.connect("e1125028","AAAaaa111","delta/decinfo.edu")	
	return oracle_connection

def creationTable():
	oracle_connection = connection()
	oracle_cursor = oracle_connection.cursor()

	table_erreur = """
		CREATE TABLE erreur(
			id NUMBER GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
			message VARCHAR2(40) NOT NULL
		)
	"""

	table_joueur = """
		CREATE TABLE joueur(
			id NUMBER GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
			nom_joueur VARCHAR2(30) NOT NULL,
			nbr_victoire NUMBER NOT NULL,
			nbr_defaites NUMBER NOT NULL,
			niveau_joueur NUMBER NOT NULL,
			UNIQUE(nom_joueur)
		)
	"""

	table_partie = """
		CREATE TABLE partie(
			id NUMBER GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
			id_type_partie NUMBER NOT NULL,
			id_joueur_gagnant NUMBER NOT NULL,
			id_joueur_perdant NUMBER NOT NULL,
			date_partie DATE,
			score_gagnant NUMBER NOT NULL,
			score_perdant NUMBER NOT NULL,
			FOREIGN KEY (id_type_partie) REFERENCES type_partie(id),
			FOREIGN KEY (id_joueur_gagnant) REFERENCES joueur(id),
			FOREIGN KEY (id_joueur_perdant) REFERENCES joueur(id)
		)
	"""

	table_type_partie = """
		CREATE TABLE type_partie(
			id NUMBER GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
			nom_partie VARCHAR2(30)
		)
	"""

	oracle_cursor.execute(table_joueur)
	oracle_cursor.execute(table_type_partie)
	oracle_cursor.execute(table_partie)

	oracle_cursor.execute("INSERT INTO joueur(nom_joueur, nbr_victoire, nbr_defaites, niveau_joueur) VALUES('invincible', 7, 2, 5)")
	oracle_cursor.execute("INSERT INTO joueur(nom_joueur, nbr_victoire, nbr_defaites, niveau_joueur) VALUES('perdant', 4, 8, 2)")

	oracle_cursor.execute("INSERT INTO type_partie(nom_partie) VALUES('libre')")
	oracle_cursor.execute("INSERT INTO type_partie(nom_partie) VALUES('une bande')")
	oracle_cursor.execute("INSERT INTO type_partie(nom_partie) VALUES('trois bandes')")



	oracle_connection.commit();
	oracle_connection.close();

def effacerTable():
	oracle_connection = connection()
	oracle_cursor = oracle_connection.cursor()
	try:
		oracle_cursor.execute("DROP TABLE partie")
		oracle_cursor.execute("DROP TABLE type_partie")
		oracle_cursor.execute("DROP TABLE joueur")
	except:
		pass

	oracle_connection.commit();
	oracle_connection.close();

def creationTrigger():
	oracle_connection = connection()
	oracle_cursor = oracle_connection.cursor()

	trigger_partie = """
		CREATE OR REPLACE TRIGGER trigger_partie
		BEFORE UPDATE OR INSERT ON partie
		FOR EACH ROW
		BEGIN
			IF :new.id_joueur_gagnant == :new.id_joueur_perdant THEN
				INSERT INTO erreur("ERREUR, Le joueur gagnant et le joueur perdant sont les même");
	"""

effacerTable()
creationTable()



		



