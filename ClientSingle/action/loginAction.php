<?php
    require_once("action/commonAction.php");
    
    class LoginAction extends CommonAction {

		public $wrongLogin = false;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC, "login");
		}

		protected function executeAction() {

			if (isset($_POST["username"])) {
				
				$data = [];
				$data["username"] = $_POST["username"];
				
				$resultat = API_DAO::callAPI("signin", $data);
				$_SESSION["visibility"] = 1;
				$_SESSION["username"] = $resultat;	
				header("location:lobby.php");
				exit;
			}
		}
	}