<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>Carambole - Connexion</title>
        <link rel="icon" href="images/ballsicon.png" type="image/x-icon" />
        <script src="js/jquery.js" charset="utf-8"></script>
        <script src="js/javascript.js" charset="utf-8"></script>
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/login.css">
    </head>

    <body>
        <script>
            let ambiance = new Audio('./music/bgTitle.ogg');
            
            
            window.onload = function () {
                ambiance.play();
            }
        </script>
        <div class="technical">
            <p>Version 0.7.3</p>
            <p>Version en développement, destinée au débogage...</p>
        </div>
        <div class="main"> 
            <header><img src="images/Title2.png"/></header>
            <div class="loginBox">
                <h1 class="login">Connexion</h1>
                <div>
                    <div class="errorMsg" ></div>
                    <div class="loginField">
                        <label class="login">Addresse serveur</label>
                        <input id="svrName" type="text" name="server" value="">
                    </div>
                    <div class="loginField">
                        <label class="login">Nom d'usager</label>
                        <input id="usrName" type="text" name="username" value="">
                    </div>
                    <div>
                        <button id="btnJouer">Entrer</button>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>