
let spriteList = [];
let player1Playing = true;
let forceCoup = 0;
let angle = 0;
let playerBallX = 0;
let playerBallY = 0;
let distanceX = 0;
let distancePreX = [];
let joueur1 = false, joueur2 = false;
var mousedownID = -1;
let iter = 0;

let ambiance = new Audio('./music/bgGame.ogg');

let ballknock1 = new Audio('./sound/ballknock1.mp3');
let bump = new Audio('./sound/bump.mp3');

window.onload = function() {

	document.getElementById("person-name").innerHTML = localStorage["username"];
	document.getElementById("player1-name").innerHTML = localStorage["username"];
	populateProfile();
	in_partie = true;
	statusPartie();
	joueur1 = true;
	console.log(joueur1);
	ambiance.play();
	ambiance.loop = true;

	spriteList.push(new Queue());
	spriteList.push(new WhiteBall());
	spriteList.push(new RedBall());
	spriteList.push(new YellowBall());

	document.getElementById("plattern").onmousemove = (event) => {

		//console.log("event x : " + event.clientX)

		findPlayerBallXandY();

		document.getElementById("clientx").textContent = event.clientX - document.getElementById("plattern").offsetLeft;;
		document.getElementById("clienty").textContent = event.clientY - document.getElementById("plattern").offsetTop;

		document.getElementById("ballx").textContent = playerBallX;
		document.getElementById("bally").textContent = playerBallY;

		document.querySelector(".queue").style.left = -(36 * Math.sin(event.clientX) - 27) + "px";
		document.querySelector(".queue").style.top = (36 * Math.cos(event.clientX) + 27) + "px";

		//console.log("queue" + -(36 * Math.sin(event.clientX) - 27))

		for (let i = 0; i < spriteList.length; i++) {

			if (spriteList[i] instanceof Queue) {

				/*document.spriteList[i].x = -(36 * Math.sin(event.clientX) - 27) + "px";
				document.spriteList[i].y = 36 * Math.cos(event.clientX) + 27 + "px";*/

				document.querySelector(".queue").style.transformOrigin="0px 50%";
				// document.querySelector(".queue").style.top = playerBallY + "px";
				// document.querySelector(".queue").style.left = playerBallX + "px";

				spriteList[i].x = playerBallX;
				spriteList[i].y = playerBallY;

				//console.log(document.querySelector(".queue").style.left);
				//spriteList[i].angle = event.clientX;
				spriteList[i].arad = Math.atan2((playerBallY - (event.clientY - document.getElementById("plattern").offsetTop)), (playerBallX - (event.clientX - document.getElementById("plattern").offsetLeft)));
				// console.log("Regarde mon radiant!! " + spriteList[i].arad);
				
				//document.querySelector(".queue").style.transform = "rotate(" + -(event.clientX) + "deg)";

				/*if (event.clientX > playerBallX) { // À gauche
					//spriteList[i].x = (playerBallX + 25) + event.clientX / 10;
					document.querySelector(".queue").style.transform = "rotate(" + -(event.clientX) + "deg)";
					//document.querySelector(".queue").style.transform = "origin(200px 400px)";
					//document.querySelector(".queue").style.transform = "origin(" + -(event.clientX) + "%)";
					
				}
				else if (event.clientX < playerBallX) { // À droite
					//spriteList[i].x = playerBallX - event.clientX / 10;
					document.querySelector(".queue").style.transform = "rotate(" + event.clientX + "deg)";
					//document.querySelector(".queue").style.transform = "origin(200px 400px)";
					//document.querySelector(".queue").style.transform = "origin(" + -(event.clientX) + "%)";
				}

				if (event.clientY < playerBallY) { // En haut
					//spriteList[i].y = (playerBallY - 50) + event.clientY / 10;
					document.querySelector(".queue").style.transform = "rotate(" + event.clientX + "deg)";
					//document.querySelector(".queue").style.transform = "origin(200px 400px)";
					//document.querySelector(".queue").style.transform = "origin(" + -(event.clientX) + "%)";
				}
				else if (event.clientY > playerBallY) { // En bas
					//spriteList[i].y = (playerBallY - 25) - event.clientY / 10;
					document.querySelector(".queue").style.transform = "rotate(" + -(event.clientX) + "deg)";
					//document.querySelector(".queue").style.transform = "origin(200px 400px)";
					//document.querySelector(".queue").style.transform = "origin(" + -(event.clientX) + "%)";
				}*/

				/*spriteList[i].x = playerBallX;
				spriteList[i].y = playerBallY;*/
			}
		}

		/*for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof Queue) {
				spriteList[i].x = event.clientX;
				spriteList[i].y = event.clientY;
			}
		}
		
		if (isPlaying) {
			// Animer la queue de billard
			for (let i = 0; i < spriteList.length; i++) {
				if (spriteList[i] instanceof Queue) {
					spriteList[i].x = event.clientX;
					spriteList[i].y = event.clientY;
				}
			}
		}*/

	}
	/*document.onmouseup = () => {


		// document.querySelector(".queue").style.transform = "rotate(90deg)";

		console.log("chis enhaut!!!")
		document.onmousemove = (event) => {
			
			//console.log("event x : " + event.clientX)

			console.log(document.querySelector(".queue").style.left);
			console.log(event.clientX);
			
			for (let i = 0; i < spriteList.length; i++) {
				if (spriteList[i] instanceof Queue) {
					console.log("queue");
					spriteList[i].x = event.clientX - canvas.offsetLeft - 100;
					spriteList[i].y = event.clientY - canvas.offsetTop - 110;
					spriteList[i].tick();
				}
			}
					
			if (isPlaying) {
				// Animer la queue de billard
				for (let i = 0; i < spriteList.length; i++) {
					if (spriteList[i] instanceof Queue) {
						spriteList[i].x = event.clientX;
						spriteList[i].y = event.clientY;
					}
				}
			}
		
		}
	}
*/
	// Décider la force du coup
	document.addEventListener("mousedown", mousedown);
	document.addEventListener("mouseup", mouseup);
	//document.addEventListener("mouseout", mouseup);

	generalTick();

}

function mousedown(event) {
	console.log("mousedown :P")
	mousedownID = setInterval(whilemousedown, 20);
	
}

function mouseup(event) {
	// action avec force du coup
	// console.log("yo");
	
	 
	for (let i = 0; i < spriteList.length; i++) {
		if (spriteList[i] instanceof Queue) {
			spriteList[i].dst = 50;
			angle = spriteList[i].arad;
			console.log("Ramène la queue")
		}
	}
	// call ajax frapperCoup() avec infos du coup -> force, nom joueur, posx, posy, etc
	clearInterval(mousedownID);
	
	if(joueur1)
	{
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof WhiteBall) {
				ballknock1.play();
				//spriteList[i].speedx = 7;
				spriteList[i].speedx = Math.cos(angle) * forceCoup;
				spriteList[i].speedy = Math.sin(angle) * forceCoup;
				console.log("change ball speed");
			}
		}
	}

	else if(joueur2)
	{
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof YellowBall) {
				ballknock1.play();
				//spriteList[i].speedx = 7;
				spriteList[i].speedx = Math.cos(angle) * forceCoup;
				spriteList[i].speedy = Math.sin(angle) * forceCoup;
				console.log("change ball speed");
			}
		}
	}

	forceCoup = 0;
	angle = 0;
}

function whilemousedown() {
	if (forceCoup < 100 ) { 
		forceCoup++;
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof Queue) {
				spriteList[i].dst++;
			}
		}
	}
	console.log("Force du coup : " + forceCoup);
}

function findPlayerBallXandY() {
	if (joueur1) {
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof WhiteBall) {
				playerBallX = spriteList[i].x;
				playerBallY = spriteList[i].y;
			}
		}
	}
	else if (joueur2) {
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof YellowBall) {
				playerBallX = spriteList[i].x;
				playerBallY = spriteList[i].y;
			}
		}
	}
}

// Animer les sprites
function generalTick()  {

	// console.log(joueur1);
	
	for (let i = 0; i < spriteList.length; i++) {
		let alive = spriteList[i].tick();
	
		if (!alive) {
			spriteList.splice(i, 1); 
			i--;
		}
	}

	//staticCollision();
	//ballCollision();

	// console.log(spriteList);

	// Change l'affichage de l'écran
	window.requestAnimationFrame(generalTick);

}

//********************** 							Fonctions de collision								*****************************

function staticCollision() {
    for (var obj1 in spriteList) {
        for (var obj2 in spriteList) {
			//console.log(spriteList[obj1].x)
			console.log(distance(spriteList[obj1], spriteList[obj2]))
            if (/*!obj1 instanceof Queue && !obj2 instanceof Queue && */obj1 !== obj2 && distance(spriteList[obj1], spriteList[obj2]) < spriteList[obj1].radius + spriteList[obj2].radius) {

				//verifBalls();
				console.log(obj1)
                var theta = Math.atan2((spriteList[obj1].y - spriteList[obj2].y), (spriteList[obj1].x - spriteList[obj2].x));
                var overlap = spriteList[obj1].radius + spriteList[obj2].radius - distance (spriteList[obj1], spriteList[obj2]);
                var smallerObject = spriteList[obj1].radius < spriteList[obj2].radius ? obj1 : obj2
                spriteList[smallerObject].x -= overlap * Math.cos(theta);
				spriteList[smallerObject].y -= overlap * Math.sin(theta);
            }
        }
    }
}

function ballCollision() {

    for (var obj1 in spriteList) {
		//iter++;
		
        for (var obj2 in spriteList) {
			//console.log(iter);
			// console.log(spriteList[obj1])
			// console.log(spriteList[obj1])
			// console.log(spriteList[obj2])
			//console.log(distance(spriteList[obj1], spriteList[obj2]) <= 100)
			
            if (!(spriteList[obj1] instanceof Queue) && !(spriteList[obj2] instanceof Queue) && spriteList[obj1] !== spriteList[obj2] && distance(spriteList[obj1], spriteList[obj2]) <= 2 && distance(spriteList[obj1], spriteList[obj2]) >= 1) {
				console.log("entre dans la boucle")

				//verifBalls();
				

				var theta1 = Math.atan(spriteList[obj1].speedy / spriteList[obj1].speedx);
				//console.log(spriteList[obj1].speedy)
				//console.log("Number?? " + parseFloat(theta1));
                var theta2 = Math.atan(spriteList[obj2].speedy / spriteList[obj2].speedx);
				var phi = Math.atan2(spriteList[obj2].y - spriteList[obj1].y, spriteList[obj2].x - spriteList[obj1].x);
				//console.log("phi = " + phi);
                var m1 = spriteList[obj1].mass;
                var m2 = spriteList[obj2].mass;
                var v1 = spriteList[obj1].speed();
				var v2 = spriteList[obj2].speed();
				
				//console.log(v1)

				// if(!isNaN(phi))
				// {
                // var dx1F = (v1 * Math.cos(theta1 - phi) + 2*m2*v2*Math.cos(theta2 - phi)) / (Math.cos(phi) + v1*Math.sin(theta1-phi)) * Math.cos(phi+Math.PI/2);
                // var dy1F = (v1 * Math.cos(theta1 - phi) + 2*m2*v2*Math.cos(theta2 - phi)) / (Math.sin(phi) + v1*Math.sin(theta1-phi)) * Math.sin(phi+Math.PI/2);
                // var dx2F = (v2 * Math.cos(theta2 - phi) + 2*m1*v1*Math.cos(theta1 - phi)) / (Math.cos(phi) + v2*Math.sin(theta2-phi)) * Math.cos(phi+Math.PI/2);
				// var dy2F = (v2 * Math.cos(theta2 - phi) + 2*m1*v1*Math.cos(theta1 - phi)) / (Math.sin(phi) + v2*Math.sin(theta2-phi)) * Math.sin(phi+Math.PI/2);
				// }

				// if(!isNaN(phi))
				// {
                // var dx1F = 
                // var dy1F = 
                // var dx2F = 
				// var dy2F = 
				// }

				console.log("PIIIIII??????? " + (theta1 - phi))
				
				if(!isNaN(phi))
				{
                var dx1F = (v1 * (theta1 - phi) + 2*m2*v2*(theta2 - phi))
                var dy1F = (v1 * (theta1 - phi) + 2*m2*v2*(theta2 - phi))
                var dx2F = (v1 * (theta1 - phi) + 2*m2*v2*(theta2 - phi))
				var dy2F = (v1 * (theta1 - phi) + 2*m2*v2*(theta2 - phi))
				}


				console.log("Nan??? " + dx1F);

                // spriteList[obj1].speedx = dx1F;                
                // spriteList[obj1].speedy = dy1F;                
                // spriteList[obj2].speedx = dx2F;                
				// spriteList[obj2].speedy = dy2F;

				//spriteList[obj1].angle = 
				
				 spriteList[obj1].speedx = dx1F;                
                 spriteList[obj1].speedy = dy1F;                
                 spriteList[obj2].speedx = dx2F;                
                 spriteList[obj2].speedy = dy2F;

            }            
        }
        //wallCollision(spriteList[obj1]);
    }
}

function wallCollision(ball) {

    if (ball.x - ball.radius + ball.dx < 0 ||
        ball.x + ball.radius + ball.dx > document.getElementById("plattern").style.width) {
		ball.dx *= -1;
		verifWalls();
	}
	
    if (ball.y - ball.radius + ball.dy < 0 ||
        ball.y + ball.radius + ball.dy > document.getElementById("plattern").style.height) {
		ball.dy *= -1;
		verifWalls();
	}
	
    if (ball.y + ball.radius > document.getElementById("plattern").style.height) {
		ball.y = document.getElementById("plattern").style.height - ball.radius;
		verifWalls();
	}
	
    if (ball.y - ball.radius < 0) {
		ball.y = ball.radius;
		verifWalls();
	}
	
    if (ball.x + ball.radius > document.getElementById("plattern").style.width) {
		ball.x = document.getElementById("plattern").style.width - ball.radius;
		verifWalls();
	}
	
    if (ball.x - ball.radius < 0) {
		ball.x = ball.radius;
		verifWalls();
	}    
	
}

function distance(ob1, ob2){
	//console.log("dst!!!!")
	let distance = 0;
	let y = 0, x = 0;

	if (ob1.y < ob2.y) {
		y = ob2.y - ob1.y;
	}
	else if (ob2.y < ob1.y) {
		y = ob1.y - ob2.y;
	}

	if (ob1.x < ob2.x) {
		x = ob2.x - ob1.x;
	}
	else if (ob2.x < ob1.x) {
		x = ob1.x - ob2.x;
	}

	distance = Math.sqrt(x^2 + y^2);
	//console.log(distance);

	return distance;
}

//**********************************************																**********************************************/

function verifWalls() {
	if (localStorage["typePartie"] == "UN_BOND") {
		if (firstHit && !secondHit) {
			murCount++;
			if (murCount == 1) {
				unMur = true;
			}
			else unMur = false;
		}
	}
	else if (localStorage["typePartie"] == "TROIS_BONDS") {
		if (firstHit && !secondHit) {
			murCount++;
			if (murCount == 3) {
				troisMurs = true;
			}
			else troisMurs = false;
		}
	}
}

function verifBalls() {
	if (joueur1) {
		if (playerPlaying == localStorage["username"]) { // playing
			if (localStorage["typePartie"] == "LIBRE") {
				if (spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall) {
					firstHit ? secondHit = true : firstHit = true;
				}
			}
			else if (localStorage["typePartie"] == "UN_BOND") {
				if ((spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (unMur) secondHit = true;
				}
			}
			else if (localStorage["typePartie"] == "TROIS_BONDS") {
				if ((spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (troisMur) secondHit = true;
				}
			}
		}
		else { // not playing
			if (localStorage["typePartie"] == "LIBRE") {
				if (spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhitewBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall) {
					firstHit ? secondHit = true : firstHit = true;
				}
			}
			else if (localStorage["typePartie"] == "UN_BOND") {
				if ((spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhiteBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (unMur) secondHit = true;
				}
			}
			else if (localStorage["typePartie"] == "TROIS_BONDS") {
				if ((spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhiteBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (troisMur) secondHit = true;
				}
			}
		}
	}
	else if (joueur2) {
		if (playerPlaying == localStorage["username"]) { // playing
			if (localStorage["typePartie"] == "LIBRE") {
				if (spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhitewBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall) {
					firstHit ? secondHit = true : firstHit = true;
				}
			}
			else if (localStorage["typePartie"] == "UN_BOND") {
				if ((spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhiteBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (unMur) secondHit = true;
				}
			}
			else if (localStorage["typePartie"] == "TROIS_BONDS") {
				if ((spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof WhiteBall || spriteList[obj1] instanceof YellowBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (troisMur) secondHit = true;
				}
			}
		}
		else { // not playing
			if (localStorage["typePartie"] == "LIBRE") {
				if (spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall) {
					firstHit ? secondHit = true : firstHit = true;
				}
			}
			else if (localStorage["typePartie"] == "UN_BOND") {
				if ((spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (unMur) secondHit = true;
				}
			}
			else if (localStorage["typePartie"] == "TROIS_BONDS") {
				if ((spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof YellowBall || spriteList[obj1] instanceof WhiteBall && spriteList[obj2] instanceof RedBall)) {
					if (!firstHit) firstHit = true;
					else if (troisMur) secondHit = true;
				}
			}
		}
	}
}


function statusDemande(){
	let nom = localStorage["username"]
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "statusDemande",
			nom : nom
		}
	})
	.done(response => {
		//response = JSON.parse(response);
		console.log(response);
		if(response.message == "ATTENTE_ADVERSAIRE"){
			setTimeout(statusDemande, 2000)
		}
		else if(response.message == "TROUVEE_ADVERSAIRE"){
			in_partie = true;
			setTimeout(statusPartie, 2000)
		}
	});
}

function statusPartie(){
	if(in_partie){
		
		let nom = localStorage["username"]
		let adress = getAdress()

		let x = 0, y = 0;
		
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof Queue) {
				x = spriteList[i].x;
				y = spriteList[i].y;
			}
		}
		
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "statusPartie",
				nom : nom,
				posx : x,
				posy : y
			}
		})
		.done(response => {
			//response = JSON.parse(response);
            console.log("Statut de la partie");
			console.log(response);

			//console.log("test" + joueur1);

			if (response.joueur1 == localStorage["username"]) {
				document.getElementById("player2-name").innerHTML = response.joueur2;
				joueur1 = true;
				joueur2 = false;
			}
			else if (response.joueur2 == localStorage["username"]) {
				document.getElementById("player2-name").innerHTML = response.joueur1;
				joueur2 = true;
				joueur1 = false;
			}

			joueur1 = true; // Test de l'utilisation sans serveur
			console.log(joueur1);

			//console.log("test" + joueur1);
			//console.log(joueur2);

			if(response.etatPartie == "EN_COURS"){
				setTimeout(statusPartie, 2000)
			}
			else{
				in_partie = false;
			}
		});
	}
	
}

function frapperCoup(){

	let nom = localStorage["username"]
	let x = 0, y = 0;

	console.log("frapper le coup!!!!!!!!")

	for (let i = 0; i < spriteList.length; i++) {
		if (spriteList[i] instanceof WhiteBall) {
			spriteList[i].speedx = 1;
			spriteList[i].speedy = 1;
			console.log("change ball speed");
		}
	}

	for (let i = 0; i < spriteList.length; i++) {
		let alive = spriteList[i].tick();
	
		for (let i = 0; i < spriteList.length; i++) {
			if (spriteList[i] instanceof Queue) {
				x = spriteList[i].x;
				y = spriteList[i].y;
			}
		}
	}

	let force = forceCoup;
	let adress = getAdress()
	
	$.ajax({

			type : "GET",
			url : adress,
			data : {
				action : "frapperCoup",
				nom : nom,
				posx : x,
				posy : y,
				force : force
			}
		})
		.done(response => {
			//response = JSON.parse(response);
			console.log(response);
		});
	
}

function finPartie(){
	let nom = localStorage["username"];
	let pointGagne = Math.round(Math.random()*40)
	let pointPerd = Math.round(Math.random()*40)
	let adress = getAdress()
	
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "finPartie",
				gagnant : nom,
				pointsPerdant : pointPerd,
				pointsGagnant : pointGagne
			}
		})
		.done(response => {
			//response = JSON.parse(response);
			console.log(response);
			in_partie = false;
		});
}
