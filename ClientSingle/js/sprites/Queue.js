class Queue {

	constructor() {
        this.x = 200;
        this.initialX = this.x;
        this.y = 200;
        this.width = 600;
        this.height = 250;
        this.dst = 50;
        this.adeg = 0;
        this.arad = 0;
        // this.count = 0;
        this.newQueue = new Image()
        this.newQueue.src = "images/queue.png"

        this.node = document.createElement("div");
		// this.node.style.width = this.size + "px";
        // this.node.style.height = this.size + "px";
        this.node.className = "queue";
        this.node.style.top = this.y + "px";
        this.node.style.left = this.x + "px";
    }

	tick() {

        /*if (this.newQueue.complete) {
            ctx.drawImage(this.newQueue, this.x, this.y, this.width, this.height);
            //console.log("Queue");
        }*/

        //let aa = this.angle/360 * 2*Math.PI;
        //let aa = this.angle;

        this.angle = (this.arad + Math.PI) *180/Math.PI;
        //console.log("Angle = "+this.angle);

        // let xx = this.dst * Math.sin(this.angle);
        // let yy = this.dst * Math.cos(this.angle);
        let xx = this.dst * Math.cos(this.arad);
        let yy = this.dst * Math.sin(this.arad);
        

        this.node.style.top = this.y - yy - (30/2) +  "px";
        this.node.style.left = this.x - xx + "px";

        this.node.style.transform = "rotate(" + this.angle + "deg)";

        

        document.getElementById("plattern").appendChild(this.node);

        //console.log("Queue x : " + this.x)
        
        //console.log("Queue y : " + this.y)

        return true;

    }

}