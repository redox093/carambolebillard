class RedBall {

	constructor() {
        this.x = 800;
        this.initialX = this.x;
        this.y = 340;
        this.speedx = 0;
        this.speedy = 0;
        this.width = 50;
        this.height = 50;
        this.mass = 10;
        this.friction = 0.25;
        
        this.newRedBall = new Image()
        this.newRedBall.src = "images/redball.png"

        this.node = document.createElement("div");
		// this.node.style.width = this.size + "px";
        // this.node.style.height = this.size + "px";
        this.node.className = "ball red";
        this.node.style.top = this.y + "px";
        this.node.style.left = this.x + "px";
    }

	speed(){
        return(Math.sqrt(this.speedx^2 + this.speedy^2));
    }

	tick() {

        // FRICTION JUSQU'À CE QUE LA BALLE SOIT IMMOBILE
        if(this.speedx > 0)
            this.speedx -= this.friction;

        if(this.speedy > 0)
            this.speedy -= this.friction;

        if(this.speedx < 0)
            this.speedx += this.friction;

        if(this.speedy < 0)
            this.speedy += this.friction;

        // REBONDS HORIZONTAUX
        if(this.y <= 75 || this.y >= 570)
        {
            this.speedy = -this.speedy;
            bump.load();
            bump.play();
        }
        
        // REBONDS VERTICAUX
        if(this.x <= 75 || this.x >= 1130)
        {
            this.speedx = -this.speedx;
            bump.load();
            bump.play();
        }

        // REPOSITIONNEMENT EN FONCTION DE LA VITESSE
        this.x += this.speedx;
        this.y += this.speedy;


        // IMMOBILISATION DE LA BALLE SI SA VITESSE SE TROUVE ENTRE 0 ET 0.25
        if(Math.abs(this.speedx) < 0.25 && Math.abs(this.speedx) > 0)
            this.speedx = 0;
        if(Math.abs(this.speedy) < 0.25 && Math.abs(this.speedy) > 0)
            this.speedy = 0;


        // PRÉVENIR LE DÉPASSEMENT DE TERRAIN
        if(this.x > 1170)
            this.x -= 30
        if(this.x < 35)
            this.x += 30
        if(this.y < 35)
            this.y += 30
        if(this.y > 610)
            this.y -= 30

        

        this.node.style.top = this.y + "px";
        this.node.style.left = this.x + "px";

        document.getElementById("plattern").appendChild(this.node);

        return true;

    }
}