let tick = 0;
let attente = false;
let spin = 12;

let ambiance = new Audio('./music/bgMenu.ogg');

window.onload = function () {

	document.getElementById("person-name").innerHTML = localStorage["username"];
	populateProfile();
	ambiance.play();
	ambiance.loop = true;
	
	// // afficher le tableau des scores
	// document.getElementById("show-scores") = () => {
		
	// }

	// // déconnection
	// document.getElementById("logout") = () => {
		
	// }

	// afficher les informations du joueur
	/*document.getElementById("profil").addEventListener('click', function(event) {
		console.log("Hey!!");
		// console.log(document.getElementById("panel-loading").style.bottom);
		
		// console.log(document.getElementById("panel-loading").style.bottom);

		showPanel("profile");
	});*/

	// choix des parties
	document.getElementById("libre").addEventListener('click', function(event) {
		document.getElementById("titrePartie").innerHTML = "Libre";
		attente = true;
		showPanel("loading");
		creerPartie("LIBRE");
	});

	document.getElementById("une-bande").addEventListener('click', function(event) {
		document.getElementById("titrePartie").innerHTML = "Une Bande";
		attente = true;
		showPanel("loading");
		creerPartie("UN_BOND");
	});

	document.getElementById("trois-bandes").addEventListener('click', function(event) {
		document.getElementById("titrePartie").innerHTML = "Trois Bandes";
		attente = true;
		showPanel("loading");
		creerPartie("TROIS_BONDS");
	});

	document.getElementById("leaveGame").addEventListener('click', function(event) {
		attente = false;
		quitterPartie();
	});

	mainTick();

}

/*function hidePanel() {
	document.getElementById("panel-profile").style.display = "none";
	//document.getElementById("panel-profile").style.bottom = "-350px";
	document.getElementById("panel-loading").style.display = "none";
	//document.getElementById("panel-loading").style.bottom = "-350px";
	//console.log(document.getElementById("panel-profile").style.bottom);
}

function showPanel(pan){
	console.log("youhouuuu");
	document.getElementById("panel-" + pan).style.display = "block";
	document.getElementById("panel-" + pan).style.bottom = "350px";
	console.log(document.getElementById("panel-"+pan).style.bottom);
}*/

function mainTick() {
	
	tick++

	// document.getElementById("panel-loading").style.bottom = "51px";
	// document.getElementById("panel-loading").textContent = tick;

	window.requestAnimationFrame(mainTick);
	document.getElementById("loading-point").style.left = -(36 * Math.sin(tick/spin + 60) - 27) + "px";
	document.getElementById("loading-point").style.top = 36 * Math.cos(tick/spin + 60) + 27 + "px";

	// document.getElementById("loading-point").style.left = -(36 * Math.sin(1) - 27) + "px";
	// document.getElementById("loading-point").style.top = 36 * Math.cos(1) + 27 + "px";
	
}