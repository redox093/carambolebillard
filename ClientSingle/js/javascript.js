let in_partie = false;

function saveUsername() {
    localStorage["username"] = $("#usrName").val();
    localStorage["serverIP"] = $("#svrName").val();
}

window.onload = function () {

    if (localStorage["username"] != null) {
        $("#usrName").val(localStorage["username"]);
    }

    if (localStorage["serverIP"] != null) {
        $("#svrName").val(localStorage["serverIP"]);
    }

    document.getElementById("btnJouer").onclick = () => {
        saveUsername();
        login();
    }

}

function login(){
	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "connexion",
			nom : nom
		}
	})
	.done(response => {
        //response = JSON.parse(response);
        
        if (response.nom == localStorage["username"] || response.erreur == "USER_ALREADY_CONNECTED") {
            window.location.href = 'lobby.php';
        }
        
        console.log(localStorage["username"])
		console.log(response);
	});
}

function logout(){
	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "disconect",
			nom : nom
		}
	})
	.done(response => {
		//response = JSON.parse(response);
        console.log(response);
        
        if (response.message == "LOGOUT_SUCCESS" || response.erreur == "USER_NOT_FOUND") {
            window.location.href = 'index.php';
        }

		in_partie = false;
	});
}

function creerPartie(type){
	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "creerPartie",
			nom : nom,
			typePartie : type
		}
	})
	.done(response => {
		//response = JSON.parse(response);
        console.log(response);
        
        console.log("Creation de la partie");
		if(response.message == "ATTENTE_ADVERSAIRE"){
			setTimeout(statusDemande, 2000)
		}
		else if(response.message == "TROUVEE_ADVERSAIRE"){
            in_partie = true;
            console.log("Creation partie");
			window.location.href = 'game.php';
			//setTimeout(statusPartie, 2000)
		}
	});
}

function statusDemande() {
	let nom = localStorage["username"];
	let adress = getAdress()
	if (attente) {
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "statusDemande",
				nom : nom
			}
		})
		.done(response => {
			//response = JSON.parse(response);
			console.log(response);
			
			console.log("Statut de la DEMANDE");
			if(response.message == "ATTENTE_ADVERSAIRE"){
				setTimeout(statusDemande, 2000)
			}
			else if(response.message == "TROUVEE_ADVERSAIRE"){
				in_partie = true;
				console.log("Statut de la demande");
				window.location.href = 'game.php';
				//setTimeout(statusPartie, 2000)
			}
		});
	}
}

function afficherScore(){

    let adress = getAdress();

    $.ajax({
        type : "GET",
        url : adress,
        data : {
            action : "statJoueur"
        }
    })
    .done(response => {
		console.log(response)
		if(response.joueurs != null){
			listeJoueur = response.joueurs
			listerJoueurs()
		}
	})
}

function statusPartie(){

	if (in_partie) {

		let nom = localStorage["username"];
		let adress = getAdress()
		let x = 140;//document.getElementById("posx").value
		let y = 150;//document.getElementById("posy").value
		
		$.ajax({
			type : "GET",
			url : adress,
			data : {
				action : "statusPartie",
				nom : nom,
				posx : x,
				posy : y
			}
		})
		.done(response => {
            //response = JSON.parse(response);
            console.log("Statut de la partie");
			console.log(response);

			console.log("test" + joueur1);

			if (response.joueur1 == localStorage["username"]) {
				joueur1 = true;
				joueur2 = false;
			}
			else if (response.joueur2 == localStorage["username"]) {
				joueur2 = true;
				joueur1 = false;
			}

			console.log("test" + joueur1);
			console.log(joueur2);

			if(response.etatPartie == "EN_COURS"){
				setTimeout(statusPartie, 2000)
			}
			else{
				in_partie = false;
			}
		});
	}
}

function quitterPartie(){

	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "leaving",
			nom : nom,
		}
	})
	.done(response => {
		//response = JSON.parse(response);
		console.log("Quitter la partie");
		console.log(response);
	});

}

function getJoueur() {
	let nom = localStorage["username"];
	let adress = getAdress()
	
	$.ajax({
		type : "GET",
		url : adress,
		data : {
			action : "statJoueur",
			nom : nom,
		}
	})
	.done(response => {
		//response = JSON.parse(response);
		console.log("Quitter la partie");
		console.log(response);

		if (response.nom == localStorage["username"]) {
			localStorage["victoires"] = response.nbVictoire;
			localStorage["defaites"] = response.nbDefaite;
			localStorage["niveau"] = response.niveau;
		}

	});
}

function getAdress(){
    let ip = localStorage["serverIP"];
    let port = "50048"
    let adress  = "http:\\\\" + ip + ":" + port
    return adress  
}

function populateProfile() {

	getJoueur();
	console.log("profile populated")
	if (localStorage["username"] != null) {
        document.getElementById("profile-name").innerHTML = localStorage["username"];
	}

	if (localStorage["niveau"] != null) {
        document.getElementById("niveau").innerHTML = "Niveau " + localStorage["niveau"];
	}

	if (localStorage["victoires"] != null) {
        document.getElementById("nbVictoires").innerHTML = localStorage["victoires"] + " Victoires";
	}

	if (localStorage["defaites"] != null) {
        document.getElementById("nbDefaites").innerHTML = localStorage["defaites"] + " Défaites";
	}

}

function hidePanel(pan) {
	// document.getElementById("panel-" + pan).style.display = "none";
	document.getElementById("panel-" + pan).style.bottom = "-800px";
	
    
    // document.getElementsByTagName("main")[0].style.opacity = "1";
    document.getElementById("bg-panel").style.display = "none";
	document.getElementById("panel-" + pan).style.opacity = "1";
	
	ambiance.volume = 1;
}

function showPanel(pan){
	console.log("youhouuuu");
	document.getElementById("panel-" + pan).style.display = "block";
	document.getElementById("panel-" + pan).style.bottom = "350px";
    console.log(document.getElementById("panel-"+pan).style.bottom);
    
    // document.getElementsByTagName("main")[0].style.opacity = "0.5";
    document.getElementById("bg-panel").style.display = "block";
	document.getElementById("panel-" + pan).style.opacity = "1";
	
	ambiance.volume = 0.5;
	
}


/*
function ping(ip, callback) {

    if (!this.inUse) {

        this.status = 'unchecked';
        this.inUse = true;
        this.callback = callback;
        this.ip = ip;

        var _that = this;
        this.img = new Image();

        this.img.onload = function () {
            console.log("response");
            _that.inUse = false;
            _that.callback = "responded";
        };

        this.img.onerror = function (e) {
            if (_that.inUse) {
                console.log("error");
                _that.inUse = false;
                _that.callback = "error";
            }
        };

        this.start = new Date().getTime();
        this.img.src = "http://" + ip;

        this.timer = setTimeout(function () {
            if (_that.inUse) {
                console.log("timeout");
                _that.inUse = false;
                _that.callback = "timeout";
            }
        }, 1500);

        return "The server at address : " + this.ip + " returned " + _that.callback;

    }

}
*/