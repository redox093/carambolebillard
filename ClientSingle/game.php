<!DOCTYPE html>
<html>
    
    <head>

        <meta charset="utf-8">

        <title>Carambole - Partie</title>

        <link rel="icon" href="images/ballsicon.png" type="image/x-icon" />
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/game.css">
        
        <script src="js/jquery.js" charset="utf-8"></script>
        <script src="js/javascript.js" charset="utf-8"></script>
        <script src="js/game.js" charset="utf-8"></script>
        <script src="js/sprites/Queue.js" charset="utf-8"></script>
        <script src="js/sprites/RedBall.js" charset="utf-8"></script>
        <script src="js/sprites/WhiteBall.js" charset="utf-8"></script>
        <script src="js/sprites/YellowBall.js" charset="utf-8"></script>

    </head>

    <body>
    <div id="bg-panel"></div>

        <main>
            <nav>
                <img id="logobar" src="images/Title2.png"/>
                <p id="person-name">Cassandra</p>
                <ul id="user-actions-nav">
                <li id="profil"><a href="javascript:showPanel('profile')">Profil</a></li>
                <li><a href="javascript:logout()">Déconnexion</a></li>
                </ul>
            </nav>

            <section id="game-container">
                <p id="ballx" class="coord">0000</p>
                <p id="bally" class="coord">0000</p>
                <!-- <canvas id="plattern" width="1080" height="720" style=""></canvas> -->
                <div id="plattern" width="1080" height="720" style=""></div>
            </section>

            <p id="clientx" class="coord">0000</p>
            <p id="clienty" class="coord">0000</p>

            <section id="game-info">
                <div class="player-info one">
                    
                    <div id="player1-name" class="player-name">Alexis</div>
                    <div id="player1-score" class="player-score">25</div>
                </div>
                <div class="player-info two">
                    <div id="player2-name" class="player-name">Player 2</div>
                    <div id="player2-score" class="player-score">42</div>
                </div>

            </section>

            <!-- <div class="player-info left">
                    
                <div class="player-name">Alexis</div>
                <div class="player-score">25</div>
            </div>
            <div class="player-info right">
                <div class="player-name">Cassandra</div>
                <div class="player-score">42</div>
            </div> -->  

            </main>

            <div id="panel-profile" class="panel">
                    <div id="profile-pic"></div>
                    <h1 id="profile-name">Some user</h1>
                    <p id="niveau">0</p>
                    <p><br/></p>
                    <p id="nbVictoires">0 victoires</p>
                    <p id="nbDefaites">0 défaites</p>
                    <p><br/></p>
                    <p><a href="javascript:hidePanel('profile')">Fermer</a></p>
            </div>

    </body>

</html>